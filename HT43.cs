﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW43
{
    class HT43
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            int status = 1;
            int length = str.Length;
            char[] res = new char[length];
            int save = 0;
            for (int i =0; i < length; i++)
            {
                switch (status)
                {
                    case 1:
                        if (str[i] == 'm')
                            status = 2;
                        else
                        { 
                            res[i] = str[i];
                            status = 1;
                        }
                        break;
                    case 2:
                        if (str[i] == 'o')
                            status = 3;
                        else
                        {
                            res[i] = str[i];
                            res[i - 1] = 'm';
                            status = 1;
                        }
                        break;
                    case 3:
                        if (str[i] == 'm')
                        {
                            res[i] = 'd';
                            res[i - 1] = 'a';
                            res[i - 2] = 'd';
                            status = 1;
                        }
                        else
                        {
                            res[i] = str[i];
                            res[i - 1] = 'o';
                            res[i - 2] = 'm';
                            status = 1;
                        }
                        break;
                }
                save = i;
            }
            if (status == 2)
                res[save] = 'm';
            if (status == 3)
            {
                res[save] = 'o';
                res[save - 1] = 'm';
            }
            for (int i = 0; i < length; i++)
                Console.Write(res[i]);
            Console.ReadKey();
        }
    }
}
