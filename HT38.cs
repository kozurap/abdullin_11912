﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW38
{
    class Program
    {
        static void LekgrafCompare(string s1, string s2)
        {   
            int sum1 = 0;
            int sum2 = 0;
            for (int i = 0; i < s1.Length; i++)
                sum1 += Convert.ToInt32(s1[i]);
            for (int i = 0; i < s2.Length; i++)
                sum2 += Convert.ToInt32(s2[i]);
            //не печатайте из метода. Пусть метод возвращает int =1,0,-1 как Compare
            if (sum1 > sum2)
                Console.WriteLine(s1 + ">" + s2);
            else if (sum1 == sum2)
                Console.WriteLine(s1 + "=" + s2);
            else
                Console.WriteLine(s1 + "<" + s2);
        }
        static void Main(string[] args)
        {
            string s1 = Console.ReadLine();
            string s2 = Console.ReadLine();
            LekgrafCompare(s1, s2);
            Console.ReadKey();
        }
    }
}
