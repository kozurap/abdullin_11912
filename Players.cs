﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW55_HW56
{
    public class Players : IComparable
    {
        public int PNum;
        public int PAge;
        public string PName;
        static int LastNum = 1;
        HashSet<int> unique = new HashSet<int>(99);
        int[] uniqueCheck = new int[99];
        public string NameSetnGet
        {
            get { return PName; }
            set { PName = value; }
        }
        public Players(string name, int age, int num)
        {
            PNum = num;
            if (age > 16 && age < 21)
                PAge = age;
            else
                throw new Exception("Incorrect age");
            PName = name;
            if (!unique.Contains(num))
                unique.Add(num);
            else
            {
                throw new Exception("Player number isn't unique");
            }
        }
        public Players(string name, int age)
        {
            PName = name;
            if (age > 16 && age < 21)
                PAge = age;
            else
                throw new Exception("Incorrect age");
            uniqueCheck = unique.ToArray();
            Array.Sort(uniqueCheck);
            foreach (int e in uniqueCheck)
            {
                if (LastNum == e)
                    LastNum++;
            }
            PNum = LastNum;
        }
        public override string ToString()
        {
            return ("Имя - " + PName + " ; Возраст - + " + PAge + " ; Номер - " + PNum);
        }
        public int CompareTo(object plr)
        {
            string s;
            //почему бы здесь  не использовать метод CompareTo для string
            //return PName.CompareTo(((Players)plr).PName)
            if (((Players)plr).PName.Length > PName.Length)
            {
                s = PName;
            }
            else
            {
                s = ((Players)plr).PName;
            }
            int c = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (((Players)plr).PName[i] > PName[i])
                {
                    c = -1;
                    break;
                }
                else if (((Players)plr).PName[i] < PName[i])
                {
                    c = 1;
                    break;
                }
            }
            if (c == 0 && ((Players)plr).PName.Length > PName.Length)
            {
                return -1;
            }
            else if (c == 0 && ((Players)plr).PName.Length < PName.Length)
            {
                return 1;
            }
            else
            {
                return c;
            }
        }
    }
    class PlayerComp : IComparer<Players>
    {
        string par;
        public PlayerComp(string s)
        {
            par = s;
        }
        public int Compare(Players a, Players b)
        {
            switch (par)
            {
                case "Number":
                    {
                        if (a.PNum > b.PNum)
                        {
                            return 1;
                        }
                        else if (a.PNum < b.PNum)
                        {
                            return -1;
                        }
                        else return 0;

                    }
                case "Age":
                    {
                        if (a.PAge > b.PAge)
                        {
                            return 1;
                        }
                        else if (a.PAge < b.PAge)
                        {
                            return -1;
                        }
                        else return 0;
                    }
                default:
                    {
                        string s;
                        if (a.PName.Length > b.PName.Length)
                        {
                            s = b.PName;
                        }
                        else
                        {
                            s = a.PName;
                        }
                        int c = 0;
                        for (int i = 0; i < s.Length; i++)
                        {
                            if (a.PName[i] > b.PName[i])
                            {
                                c = 1;
                                break;
                            }
                            else if (a.PName[i] < b.PName[i])
                            {
                                c = -1;
                                break;
                            }
                        }
                        if (c == 0 && a.PName.Length > b.PName.Length)
                        {
                            return 1;
                        }
                        else if (c == 0 && a.PName.Length < b.PName.Length)
                        {
                            return -1;
                        }
                        else
                        {
                            return c;
                        }
                    }


            }
        }
    }
}
