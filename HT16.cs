﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW16
{
    class HT16
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите систему счисления");
            int k = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите Число");
            int n = Convert.ToInt32(Console.ReadLine());
            int stepk = 1;
            int x = 0;
            for (int i = 0 ; i < k ; i++)
            {
                if (n % 10 >= k)
                {
                    Console.WriteLine("Ошибка. Введенное число не соотвествует введенной системе счисления");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
                x += (n % 10) * stepk;
                stepk *= k;
                n = n / 10;
            }
            Console.WriteLine(x);
            Console.ReadKey();   
        }
    }
}
