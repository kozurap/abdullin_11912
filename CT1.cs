﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_work_1
{
    /** @author Timur Abdullin
     * 11-912
     * Control work task 1 
     */
    class CT1
    {
        static void Main(string[] args)
        {
            double x = double.Parse(Console.ReadLine());
            double eps = 0.000001;
            double y2 = 1;
            double y1;
            int n = 0;
            int factn = 1;
            int step = 4;
            int l = -1;
            double xstep = x;
            int nn = 1 ;
            //Еще раз пересмотрела все верно. При вводе попробуйте x=0,44. Тогда Sqrt(1+x)~=1,2.
            //Зачем эта проверка здесь? Без нее все хорошо работает. Чтобы время сэкономить? 
            //Я забыла в чем был вопрос на паре.
            if (x == 0)
            {
                Console.WriteLine('1');
                Console.ReadKey();
                Environment.Exit(0);
            }
            do
            {
                n++;
                nn *= (2 * n - 1) * 2 * n;
                y1 = y2;
                factn *= n;
                double mind = 1 - 2*n;
                y2 = y2 + (xstep * l * nn) / (mind * factn * factn * step);
                xstep *= x;
                l *= -1;
                step *= 4;
                
            } while (Math.Abs(y2 - y1) > eps);
            Console.WriteLine(y2);    
            Console.ReadKey();
        }
    }
}
