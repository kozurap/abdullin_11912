﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW19
{
    /**

* @author Timur Adbullin

* 11-912

* Task 19

*/
    class HT19
    {
        static void Main(string[] args)
        {
            int[] arr1 = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            int[] arr2 = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            int index = 0;
            if (arr1.Length != arr2.Length)
            {
                Console.WriteLine("Ошибка, невозможно перемножить подстановки");
                Environment.Exit(0);
            }
            for (int i = 0; i < arr1.Length; i++)
            {
                index = arr2[i];
                try
                {
                    //что-то тут не так. Посмотрите https://www.youtube.com/watch?v=4KG_JYhx0IY
                    //надо было результат писать в новый массив, тогда бы нормально все было.
                    arr1[i] = arr1[index];
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("Ошибка, невозможно перемножить подстановки");
                    Environment.Exit(0);
                }
                Console.Write(arr1[i]);
                Console.Write(' ');
            }
            Console.ReadKey();
        }
    }
}
