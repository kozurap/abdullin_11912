﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FiguresAreMoving
{
    public partial class Form1 : Form
    {
        Timer timer = new Timer();
        public Form1()
        {
            InitializeComponent();
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.KeyCode == Keys.W)
            {
                Dot.y -= 1;
            }
            if (e.KeyCode == Keys.D)
            {
                Dot.x += 1;
            }
            if (e.KeyCode == Keys.A)
            {
                Dot.x -= 1;
            }
            if (e.KeyCode == Keys.S)
            {
                Dot.y += 1;
            }
            if (e.KeyCode == Keys.V)
            {
                Figure.VisabilityChange();
            }
        }
        public void TimerTick(object sender, EventArgs e)
        {
            Invalidate();
        }
        void Form1_Load(object sender, EventArgs e)
        {
            NiceMessageBox.CreateDialog("Choose Figure", ("Point", OnFigureChoose), ("Rectangle", OnFigureChoose), ("Circle", OnFigureChoose));
        }
       
        public void OnFigureChoose(object sender, EventArgs e)
        {
            if (sender is Button button)
            {
                if (button.Text == "Point")
                {
                    Figure.DotHasAnObject = true;
                }
                if (button.Text == "Circle")
                {
                    Figure.CircleHasAnObject = true;
                    Circle.r = 10;
                }
                if (button.Text == "Rectangle")
                {
                    Figure.RectangleHasAnObject = true;
                    Rectangle.a = 10;
                    Rectangle.b = 20;
                }
                timer.Interval = 30;
                DoubleBuffered = true;
                timer.Tick += TimerTick;
                timer.Start();
                Text = "Moving figures";
            }
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics g = e.Graphics;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            for (int i = 0; i < 15; i++)
                for (int j = 0; j < 15; j++)
                {
                    {
                        if (Figure.IsVisible)
                        {
                            if (Figure.DotHasAnObject)
                            {
                                if (Figure.Color == "Красный")
                                    g.FillEllipse(Brushes.Red,Dot.x,Dot.y, 1, 1);
                                if (Figure.Color == "Синий")
                                    g.FillEllipse(Brushes.Blue, Dot.x, Dot.y, 1, 1);
                            }
                            if (Figure.RectangleHasAnObject)
                            {
                                if (Figure.Color == "Красный")
                                    g.FillRectangle(Brushes.Red,Dot.x,Dot.y, Rectangle.a, Rectangle.b);
                                if (Figure.Color == "Синий")
                                    g.FillRectangle(Brushes.Blue, Dot.x, Dot.y, Rectangle.a, Rectangle.b);
                            }
                            if (Figure.CircleHasAnObject)
                            {
                                if (Figure.Color == "Красный")
                                    g.FillEllipse(Brushes.Red, Dot.x, Dot.y, Circle.r, Circle.r);
                                if (Figure.Color == "Синий")
                                    g.FillEllipse(Brushes.Blue, Dot.x, Dot.y, Circle.r, Circle.r);
                            }
                        }
                    }
                }

        }


        private void Label1_Click(object sender, EventArgs e)
        {
            if (Figure.CircleHasAnObject)
                label1.Text = (Circle.r * Circle.r * Math.PI).ToString();
            else if (Figure.RectangleHasAnObject)
                label1.Text = (Rectangle.a * Rectangle.b).ToString();
            else
                label1.Text = "Фируга - Точка";
        }

        private void Label3_Click(object sender, EventArgs e)
        {
            Figure.Color = "Синий";
        }

        private void Label6_Click(object sender, EventArgs e)
        {
            Circle.r++;
        }

        private void Label2_Click(object sender, EventArgs e)
        {
            Figure.Color = "Красный";
        }

        private void Label4_Click(object sender, EventArgs e)
        {
            Rectangle.a++;
        }

        private void Label5_Click(object sender, EventArgs e)
        {
            Rectangle.b++;
        }
    }
    public class NiceMessageBox
    {
        public static void CreateDialog(string heading, params (string label, EventHandler eventHandler)[] buttons)
        {
            Form messageForm = new Form();
            messageForm.Text = heading;
            int width = 100 + 80 * buttons.Length;
            int height = 100;
            messageForm.ClientSize = new Size(width, height);
            messageForm.FormBorderStyle = FormBorderStyle.FixedDialog;
            messageForm.StartPosition = FormStartPosition.CenterParent;
            messageForm.ControlBox = false;
            int i = 0;
            foreach (var param in buttons)
            {
                Button button = new Button();
                button.Text = param.label;
                button.AutoSize = true;
                button.Location = new Point(50 + i * 80, 50);
                button.Click += param.eventHandler;
                button.Click += (o, e) => messageForm.Close();
                messageForm.Controls.Add(button);
                i++;
            }
            messageForm.ShowDialog();
        }
    }

    abstract class Figure
    {
        public static string Color = "Красный";
        public static bool IsVisible = true;
        public static bool RectangleHasAnObject = false;
        public static bool DotHasAnObject = false;
        public static bool CircleHasAnObject = false;

        public static void VisabilityChange()
        {
            IsVisible = !IsVisible;
        }
    }
    class Dot : Figure
    {
        public static float x=0;
        public static float y=0;
    }
    class Rectangle : Dot
    {
        public static float a;
        public static float b;
    }
    class Circle : Dot
    {
        public static float r;
    }
}
