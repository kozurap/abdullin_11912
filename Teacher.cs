﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW48
{
    class Teacher
    {
        string FIO;
        string subject;
        public Teacher (string name, string subj)
        {
            FIO = name;
            subject = subj;
        }
        public override string ToString()
        {
            return (FIO + " " + subject);
        }
        public void ChangeSubject (string subj)
        {
            subject = subj;
        }
        public void MarkTheStudent (string SecondName)
        {
            Random rand = new Random();
            int x = rand.Next(2, 6);
            Console.Write("Преподаватель " + FIO + " оценил студента с именем " + SecondName + " По предмету " + subject + " На оценку ");
            switch (x)
            {
                case 2:
                    Console.Write("неудовлетворительно");
                    break;
                case 3:
                    Console.Write("удовлетворительно");
                    break;
                case 4:
                    Console.Write("хорошо");
                    break;
                case 5:
                    Console.Write("отлично");
                    break;
            }
        }
    }
}
