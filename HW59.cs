﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW59
{
    interface ICipher
    {
        void encode();
        void decode();
    }
    class ACipher : ICipher
    {
        public string Code;

        public ACipher(string a)
        {
            Code = a.ToLower();
        }
        public ACipher()
        {
            Code = "";
        }
        public void encode()
        {
            char[] coder = Code.ToCharArray();
            for (int i = 0; i < Code.Length; i++)
            {
                if (char.IsLetter(coder[i]))
                    if (coder[i] != 'z')
                        coder[i] = Convert.ToChar(Convert.ToInt32(coder[i]) + 1);
                    else
                        coder[i] = 'a';
            }
            Code = new string(coder);
        }
        public void decode()
        {
            char[] decoder = Code.ToCharArray();
            for (int i = 0; i < Code.Length; i++)
            {
                if (char.IsLetter(decoder[i]))
                    if (decoder[i] != 'a')
                        decoder[i] = Convert.ToChar(Convert.ToInt32(decoder[i]) - 1);
                    else
                        decoder[i] = 'z';
            }
            Code = new string(decoder);
        }
    }
    class BCipher : ICipher
    {
        public string Code;

        public BCipher(string a)
        {
            Code = a.ToLower();
        }
        public BCipher()
        {
            Code = "";
        }
        public void encode()
        {
            char[] coder = Code.ToCharArray();
            for(int i =0; i < Code.Length; i++)
            {
                if (char.IsLetter(coder[i]))
                    coder[i] = Convert.ToChar((int)'z' + (int)'a' - Convert.ToInt32(coder[i]));  
            }
            Code = new string(coder);
        }
        public void decode()
        {
            char[] decoder = Code.ToCharArray();
            for (int i = 0; i < Code.Length; i++)
            {
                if (char.IsLetter(decoder[i]))
                    decoder[i] = Convert.ToChar(-(-(int)'z'+ Convert.ToInt32(decoder[i]))+(int)'a');
            }
            Code = new string(decoder);
        }
    }
    class HW59
    {
        static void Main(string[] args)
        {
            ACipher z1 = new ACipher("Hi");
            BCipher z2 = new BCipher("ABC");
            z1.encode();
            z2.encode();
            Console.WriteLine(z1.Code + " " + z2.Code);
            z1.decode();
            z2.decode();
            Console.WriteLine(z1.Code + " " + z2.Code);
            Console.ReadKey();
        }
    }
}
