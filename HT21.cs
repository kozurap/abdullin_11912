﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW21
{
    /**

* @author Timur Abdullin

* 11-912

* Task 21

*/
    class HT21
    {
        static void Main(string[] args)
        {
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            int left = 0;
            int right = arr.Length-1;
            int servar = int.Parse(Console.ReadLine());
            while (left <= right)
            {
                int middle = (left + right) / 2;
                if (servar == arr[middle])
                {
                    Console.WriteLine(middle);
                    Console.ReadKey();
                    Environment.Exit(0);
                }
                else if (servar < middle)
                    right = middle - 1;
                else left = middle + 1;
            }
            Console.WriteLine("Такого числа в массиве не найдено");
            Console.ReadKey();
        }
    }
}
