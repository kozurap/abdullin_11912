﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW27
{
    /**

* @author Abdullin Timur

* 11-912

* Task 25

*/
    class HT27
    {
        static void Main(string[] args)
        {
            int m = int.Parse(Console.ReadLine());
            int n = int.Parse(Console.ReadLine());
            int m2 = int.Parse(Console.ReadLine());
            int n2 = int.Parse(Console.ReadLine());
            int[,] arr = new int[m, n];
            int[,] arr2 = new int[m2, n2];
            int[,] answ = new int[m, n2];
            int[] prob2 = new int[n];
            int[] prob1 = new int[n];
            int sum = 0;
            if (n != m2)
            {
                Console.WriteLine("Совершить умножение невозможно");
                Console.ReadKey();
                Environment.Exit(0);
            }
            for (int i = 0; i < m; i++)
            {
                string[] s = Console.ReadLine().Split(' ');
                for (int j = 0; j < n; j++)
                {
                    arr[i, j] = int.Parse(s[j]);
                }
            }
            for (int i = 0; i < m2; i++)
            {
                string[] s = Console.ReadLine().Split(' ');
                for (int j = 0; j < n2; j++)
                {
                    arr2[i, j] = int.Parse(s[j]);
                }
            }
            Console.WriteLine();
            //зачем массивы prob1 и prob2? 
            //answ[i,g]+=arr[i,j]*arr2[j,g]
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                    prob1[j] = arr[i, j];
                for (int g = 0; g < n2; g++)
                {
                    sum = 0;
                    for (int j = 0; j < m2; j++)
                    {
                        prob2[j] = arr2[j, g];
                        sum += prob2[j] * prob1[j];
                    }
                    answ[i, g] = sum;
                }
            }
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n2; j++)
                    Console.Write(answ[i, j] + " ");
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
