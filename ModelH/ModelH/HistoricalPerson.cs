﻿using System;

namespace ModelH
{
    public class HistoricalPerson
    {
        public string Id { get; set; }
        public string Birthday { get; set; }
        public string DateOfDeath { get; set; }
        public string Country { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string SecondName { get; set; }

        public HistoricalPerson(string id, string birthday, string dateOfDeath, string country, string name,
            string surname, string second_name)
        {
            Id = id;
            Birthday = birthday;
            DateOfDeath = dateOfDeath;
            Country = country;
            Name = name;
            Surname = surname;
            SecondName = second_name;
        }

        public HistoricalPerson()
        {
            
        }
    }
}