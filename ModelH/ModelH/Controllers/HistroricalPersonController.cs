﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using static ModelH.HistoricalPerson;
using System.IO;
using Microsoft.VisualBasic.CompilerServices;

namespace ModelH.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HistroricalPersonController : ControllerBase
    {
        private List<HistoricalPerson> _historicalPersons = new List<HistoricalPerson>
        {
            new HistoricalPerson("1", "1952,10,7", "-", "Russia", "Vladimir", "Putin", "Vladimirovich"),
            new HistoricalPerson("2", "1530,8,25", "1584,3,18", "Kievan Rus", "Ivan", "Rurekovich", "Vasilevich"),
            new HistoricalPerson("3", "1869,10,2", "1948,1,30", "India", "Mahatma", "Gandhi", "-"),
            new HistoricalPerson("4", "1925,5,19", "1998,4,15", "Cambodia", "Pol", "Pot", "-"),
            new HistoricalPerson("5", "1921,2,22", "1996,11,3", "Central African Republic", "Jean-Bidel", "Bokassa",
                "-")
        };

        [HttpGet]
        public List<HistoricalPerson> GetList() => _historicalPersons;

        [HttpGet("{Id}")]
        public HistoricalPerson GetById([FromRoute] int Id) => _historicalPersons.FirstOrDefault(p => p.Id.Equals(Id));

        [HttpPost]
        public List<HistoricalPerson> Post(HistoricalPerson person)
        {
            try
            {
                int k = 1;
                foreach (var e in _historicalPersons)
                {
                    k++;
                }
                if (person.Id != null)
                    if (int.Parse(person.Id) < k)
                        person.Id = (k).ToString();
                _historicalPersons.Add(person);
                return _historicalPersons;
            }
            catch
            {
                Console.WriteLine("Invalid input");
                return null;
            }
        }

        [HttpPut]
        public HistoricalPerson Put(HistoricalPerson person)
        {
            var _hisPerson = _historicalPersons.SingleOrDefault(p => p.Id.Equals((person.Id)));
            _hisPerson = person;
            return _historicalPersons.SingleOrDefault(p => p.Id.Equals((person.Id)));
        }

        [HttpDelete("{Id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            _historicalPersons.Remove(_historicalPersons.SingleOrDefault(p => p.Id.Equals(id)));
            return Ok();
        }
        
    }
}