﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nodes
{
     public class CustomArrayList<T> : IEnumerable<T>, ICustomCollection<T> where T : IComparable<T>
    {
        T[] arr;
        int count;
        public CustomArrayList(T[] mas)
        {
            arr = new T[mas.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                if (mas[i] != null)
                {
                    arr[i] = mas[i];
                    count++;
                }
            }
        }
        public void Add(T value)
        {
            if (count + 1 > arr.Length)
            {
                T[] temp = new T[2 * arr.Length];
                temp[arr.Length] = value;
                for (int i = 0; i < arr.Length; i++)
                    temp[i] = arr[i];
                arr = temp;
            }
            else
                arr.Append(value);
        }
        public void AddRange(T[] value)
        {
            if (count + value.Count() > arr.Length)
            {
                T[] temp = new T[arr.Length * 2];
                for (int i = 0; i < arr.Length; i++)
                    temp[i] = arr[i];
                for (int i = 0; i < value.Length; i++)
                    Add(value[i]);
            }
            else
                for (int i = 0; i < value.Length; i++)
                    Add(value[i]);
        }
        public int Size()
        {
            return count;
        }
        public bool IsEmpty()
        {
            return (count == null);
        }
        public bool Contains(T value)
        {
            return (arr.Contains(value));
        }
        public void Clear()
        {
            arr = null;
        }
        public int IndexOf(T data)
        {
            int i = 0;
            while (arr[i].CompareTo(data)!=0)
            {
                if (i < arr.Count())
                    i++;
                else
                    return -1;
            }
            return i;
        }
        public void Insert(int index, T data)
        {
            if(arr.Length == arr.Count())
            {
                T[] temp = new T[arr.Length];
                arr.Concat(temp);
            }
            for (int i = arr.Count(); i>index - 1; i++)
            {
                arr[i] = arr[i - 1];
            }
            arr[index] = data;
        }
        public void Remove(T data)
        {
            int j = 0;
            while (arr[j].CompareTo(data) != 0)
            {
                if (j < arr.Count())
                {
                    j++;
                }
                else
                    return;
            }
            for(int i = j; i < arr.Count()-1; i++)
            {
                arr[i] = arr[i + 1];
            }
            arr[arr.Count()-1] = default(T);
        }
        public void RemoveAll(T data)
        {
            for(int i = 0; i < arr.Count(); i++)
            {
                if(arr[i].CompareTo(data) == 0)
                {
                    for(int j =i;j<arr.Count()-1; j ++)
                    {
                        arr[j] = arr[j + 1];
                    }
                    arr[arr.Count() - 1] = default(T);
                }
            }
        }
        public void RemoveAt(int index)
        {
            for (int i = index; i < arr.Count() - 1; i++)
            {
                arr[i] = arr[i + 1];
            }
            arr[arr.Count() - 1] = default(T);
        }
        public void Reverse()
        {
            arr.Reverse();
        }
        public T this[int index]
        {
            get
            {
                return arr[index];
            }
            set
            {
                arr[index] = value;
            }
        }
        public IEnumerator<T> GetEnumerator()
        {
            return new ArrayListEnumerator<T>(arr);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
    public class ArrayListEnumerator<T> : IEnumerator<T>
    {
        T[] arr;
        int i = 0;
        public ArrayListEnumerator(T[] ar)
        {
            arr = ar;
        }
        T IEnumerator<T>.Current
        {
            get
            {
                if (arr != null)
                    return arr[i];
                else throw new NullReferenceException();
            }
        }
        object IEnumerator.Current { get { return arr[i]; } }
        public bool MoveNext()
        {
            if (i < arr.Count() - 1) 
            {
                i++;
                return true;
            }
            else
                return false;
        }
        public void Reset()
        {
            arr = null;
        }
        public void Dispose()
        {

        }
    }
}
