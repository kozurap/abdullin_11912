﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nodes
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] NodeArr = { 5, 6, 1, 1, 3, 2, 7, 11, 5 };
            NodeList<int> nodeList = new NodeList<int>(NodeArr);
            Console.WriteLine(nodeList.Size());
            nodeList.AddRange(new int[] { 2,3,4 });
            nodeList.WriteNode();
            Console.WriteLine(nodeList.IndexOf(11));
            nodeList.Insert(3, 18);
            nodeList.WriteNode();
            nodeList.RemoveAt(6);
            nodeList.WriteNode();
            nodeList.RemoveAll(1);
            nodeList.WriteNode();
            nodeList.Remove(6);
            nodeList.WriteNode();
            nodeList.Reverse();
            nodeList.WriteNode();
            NodeArr = new int[]{ 5, 6, 1, 1, 3, 2, 7, 11, 5 };
            DoubleNodeList<int> nodeList2 = new DoubleNodeList<int>(NodeArr);
            Console.WriteLine(nodeList2.Size());
            nodeList2.AddRange(new int[] { 2,3, 4 });
            nodeList2.WriteNode();
            Console.WriteLine(nodeList2.IndexOf(11));
            nodeList2.Insert(3, 18);
            nodeList2.WriteNode();
            nodeList2.RemoveAt(6);
            nodeList2.WriteNode();
            nodeList2.RemoveAll(1);
            nodeList2.WriteNode();
            nodeList2.Remove(6);
            nodeList2.WriteNode();
            nodeList2.Reverse();
            nodeList2.WriteNode();
            Console.ReadKey();
            int[] ar = { 1, 2, 3 };
            CustomArrayList<int> arr = new CustomArrayList<int>(ar);
        }
    }
}
