﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nodes
{
    public class DoubleNode<T> 
    {
        public T Data { get; set; }
        public DoubleNode<T> NextNode { get; set; }
        public DoubleNode<T> PrevNode { get; set; }
        public DoubleNode(T data, DoubleNode<T> nextNode, DoubleNode<T> prevNode)
        {
            Data = data;
            NextNode = nextNode;
            PrevNode = prevNode;
        }
    }
}
