﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nodes
{
    interface ICustomCollection<T>
    {
        int Size();
        bool IsEmpty();
        bool Contains(T data);
        void Add(T data);
        void AddRange(T[] data);
        void Remove(T data);
        void RemoveAll(T data);
        void RemoveAt(int Index);
        void Clear();
        void Reverse();
        void Insert(int Index, T item);
        int IndexOf(T item);

    }
}
