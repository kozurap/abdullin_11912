﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW28
{
/* 
@author Abdullin Timur
11-912
Task  28 
*/
    class Program
    {
        static bool diagch(int[,] matrix, int n,int m) /*Метод возвращает true если побочная диагональ, которую он рассматривает, четная, иначе false*/
        {
            int sum = 0;
            for (int i =0; i < m; i++)
            {
                sum+= matrix[i,n];
                n--;
                if (n < 0)
                    n = m-1;
            }
            if (sum % 2 == 0)
                return true;
            else return false;
        }
        static void Main(string[] args)
        {
            int m = int.Parse(Console.ReadLine());
            int n = int.Parse(Console.ReadLine());
            bool chetko = true;
            int[,] arr = new int[m, n];
            for (int i = 0; i < m; i++)
            {
                string[] s = Console.ReadLine().Split(' ');
                for (int j = 0; j < n; j++)
                {
                    arr[i, j] = int.Parse(s[j]);
                }
            }
            for (int i = 0; i < m; i++)
            {
                chetko = diagch(arr, i, m);
                if (chetko == false)
                    break;
            }
            if (chetko == true)
                Console.WriteLine("Все побочные диагонали четные");
            else Console.WriteLine("Не все побочные диагонали четные");
            Console.ReadKey();
        }
    }
}
