﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_task_3
{
    class CT3
    {
        static void Main(string[] args)
        {
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            int sum1 = 0;
            int sum2 = 0;
            int x=0;
            int y=0;
            for (int i = a; i <= b; i++)
            {
                x = i;
                for (int j = 1; j <= (i + 1) / 2; j++)
                    if (i % j == 0)
                        sum1 += j;
                sum1 += x;
                for (int j=i+1; j <= b; j++)
                {
                    y = j;
                    for (int g = 1; g <= (j + 1) / 2; g++)
                        if (j % g == 0)
                            sum2 += g;
                    sum2 += y;
                    if (sum1 == sum2)
                        Console.WriteLine(x + " " + y);
                    sum2 = 0;
                }
                sum1 = 0;
            }
            Console.ReadKey();
        }
    }
}
