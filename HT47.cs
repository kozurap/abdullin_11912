﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace W47
{
    class HT47
    {
        static void Main(string[] args)
        {
            int status = 0;
            string s =Console.ReadLine();
            int length = s.Length;
            for (int i = 0; i < length; i++)
            {
                switch (status)
                {
                    case 0:
                        if (s[i] == '0')
                            status = 1;
                        else if (s[i] == '1')
                            status = 5;
                        else status = -2;
                        break;
                    case 1:
                        if (s[i] == '0')
                            status = 2;
                        else if (s[i] == '1')
                            status = 3;
                        else
                            status = -2;
                        break;
                    case 2:
                        if (s[i] == '0')
                            status = 2;
                        else if (s[i] == '1')
                            status = -1;
                        else
                            status = -2;
                        break;
                    case 3:
                        if (s[i] == '0')
                            status = 4;
                        else if (s[i] == '1')
                            status = -1;
                        else
                            status = -2;
                        break;
                    case 4:
                        if (s[i] == '1')
                            status = 3;
                        else if (s[i] == '0')
                            status = -1;
                        else
                            status = -2;
                        break;
                    case 5:
                        if (s[i] == '1')
                            status = 6;
                        else if (s[i] == '0')
                            status = 7;
                        else
                            status = -2;
                        break;
                    case 6:
                        if (s[i] == '1')
                            status = 6;
                        else if (s[i] == '0')
                            status = -1;
                        else
                            status = -2;
                        break;
                    case 7:
                        if (s[i] == '1')
                            status = 8;
                        else if (s[i] == '0')
                            status = -1;
                        else
                            status = -2;
                        break;
                    case 8:
                        if (s[i] == '0')
                            status = 7;
                        else if (s[i] == '1')
                            status = 9;
                        else
                            status = -2;
                        break;
                }
                if (status == -1)
                {
                    Console.WriteLine("Строка неправильная");
                    Console.ReadKey();
                    return;
                }
                if (status == -2)
                {
                    Console.WriteLine("Введено не двоичное число");
                    Console.ReadKey();
                    return;
                }
            }
            Console.WriteLine("Строка правильная");
            Console.ReadKey();
        }
    }
}
