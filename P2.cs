﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P2
{
    class Program
    {
        static void WriteGrey(int[,] arr, int n)
        {
            int count = 0;
            for (int i = 0; i <= n / 2; i++)
            {
                for (int j = n / 2 - count; j <= n / 2 + count; j++)
                {
                    Console.WriteLine(arr[i, j]);
                }
            count++;
            }
            count = 1;
            for (int i = n / 2 + 1; i < n; i++)
            {
                for (int j = 0 + count; j < n - count; j++)
                {
                    Console.WriteLine(arr[i, j]);
                }
                count++;
            }
        }
        static void Main(string[] args)
        {
        }
    }
}
