﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C8
{
    class C8_Person
    {
        private string name { get; }
        private int passport_data { get; }
        private string phone_number { get; }
        public C8_Person()
        {
            name = "undentyfied";
            passport_data = 0;
            phone_number = "Number";
        }
        public C8_Person(string name)
        {
            this.name = name;
            passport_data = 0;
            phone_number = "Number";
        }
        public C8_Person(string name, int passport)
        {
            this.name = name;
            passport_data = passport;
            phone_number = "Number";
        }
        public C8_Person(string name, int password, string number)
        {
            this.name = name;
            passport_data = password;
            phone_number = number;
        }
        public override string ToString()
        {
            return (name + ";" + passport_data + ";" + phone_number);
        }

    }
    interface IPrint
    {
        void Print();
    }
    class BankAccount:IPrint
    {
        private long bankNum { get; }
        protected long moneyCount; long mcget { get { return moneyCount; } }
        private C8_Person owner { get; }
        static long index = 10000;
        List<BankTransaction> ThisAccTrasactions= new List<BankTransaction>();
        public BankAccount()
        {
            owner = new C8_Person();
            moneyCount = 0;
            bankNum = index;
            index++;
        }
        public BankAccount(BankAccount bank)
        {
            bankNum = index;
            moneyCount = bank.moneyCount;
            owner = bank.owner;
            index++;
        }
        public BankAccount(C8_Person Owner, long money)
        {
            owner = Owner;
            moneyCount = money;
            bankNum = index;
            index++;
        }
        public virtual void AddMoney(int money)
        {
            moneyCount += money;
            BankTransaction bt = new BankTransaction(money);
            this.ThisAccTrasactions.Add(bt);
        }
        public virtual void GetMoney(int money)
        {
            if (money <= moneyCount)
            {
                moneyCount -= money;
                BankTransaction bt = new BankTransaction(-money);
                this.ThisAccTrasactions.Add(bt);
            }
            else
                Console.WriteLine("Недостаточно средств на счете");
        }
        public virtual void TranMoney(BankAccount per, int money)
        {
            if (money <= moneyCount)
            {
                moneyCount -= money;
                per.moneyCount += money;
                BankTransaction bt = new BankTransaction(-money);
                ThisAccTrasactions.Add(bt);
                BankTransaction bt2 = new BankTransaction(money);
                per.ThisAccTrasactions.Add(bt2);
            }
            else
                Console.WriteLine("Недостаточно средств на счете");
        }
        public override string ToString()
        {
            return (bankNum + ";" + owner + ";" + moneyCount);
        }
        static public bool operator ==(BankAccount b1, BankAccount b2)
        {
            if (b1.moneyCount == b2.moneyCount)
                return true;
            else return false;
        }
        static public bool operator !=(BankAccount b1, BankAccount b2)
        {
            if (b1.moneyCount == b2.moneyCount)
                return false;
            else return true;
        }
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            BankAccount acc = obj as BankAccount;
            if (acc == null)
                return false;
            if (this.moneyCount == acc.moneyCount)
                return true;
            else return false;
        }
        void IPrint.Print()
        {
            Console.WriteLine("Ваш баланс" + moneyCount);
        }
        public BankTransaction this[BankTransaction bt]
        {
            get
            {
                foreach( BankTransaction e in ThisAccTrasactions)
                {
                    if (e == bt)
                        return e;
                }
                return null;
            }
        }
    }
    class BankTransaction
    {
        readonly DateTime date;
        readonly int moneyinTransaction;
        public BankTransaction(int money)
        {
            date = DateTime.Now;
            moneyinTransaction = money;
        }
    }
    class BankAccountPlus : BankAccount
    {
        readonly int percent;
        readonly int HowMuchCanIGet;
        public BankAccountPlus( C8_Person Owner, long money, int per, int HMCIG): base (Owner, money)
        {
            percent = per;
            HowMuchCanIGet = HMCIG;
        }
        public BankAccountPlus(int per, int HMCIG): base()
        {
            percent = per;
            HowMuchCanIGet = HMCIG;
        }
        public BankAccountPlus() : base()
        {
            percent = 0;
            HowMuchCanIGet = 0;
        }
        public void plusmoney()
        {
            moneyCount += moneyCount * percent;
        }
        public override void GetMoney(int money)
        {
            if (money<=HowMuchCanIGet)
            base.GetMoney(money);
        }
        public override void TranMoney(BankAccount per, int money)
        {
            if(money<=HowMuchCanIGet)
            base.TranMoney(per, money);
        }
    } 
}
