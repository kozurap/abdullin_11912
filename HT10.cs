﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW10
{
    class HT10
    {
        /**

* @author Abdullin Timur

* 11-912

* Task 10

*/
        static int Fact(int l)
        {
            int x = 1;
            for (int i=1; i < l; i++)
            {
                x *= i;
            }
            return x;
        }
        static void Main(string[] args)
        {
            double y = 1;
            double x = Convert.ToDouble(Console.ReadLine());
            int n = Convert.ToInt32(Console.ReadLine());
            for (int i = 1; i < n + 1;i++)
            {
                y += (Math.Pow(x, i) /Fact(i)); // красиво, коротко и понятно, но не рационально. 
            }
            Console.WriteLine(y);
            Console.ReadKey();
        }
    }
}
