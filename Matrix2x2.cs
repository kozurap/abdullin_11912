﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW50
{
    class Matrix2x2
    {
        double[,] matrix = new double[2, 2];
        public Matrix2x2()
        {
            for (int i = 0; i < 4; i++)
                matrix[i / 2, i % 2] = 0;
        }
        public Matrix2x2(double[] arr)
        {
            for (int i = 0; i < 4; i++)
                matrix[i / 2, i % 2] = arr[i];
        }
        public Matrix2x2(double a)
        {
            for (int i = 0; i < 4; i++)
                matrix[i / 2, i % 2] = a;
        }
        public Matrix2x2(double a1, double a2, double a3, double a4)
        {
            matrix[0, 0] = a1;
            matrix[0, 1] = a2;
            matrix[1, 0] = a3;
            matrix[1, 1] = a4;
        }
        public static Matrix2x2 operator +(Matrix2x2 a, Matrix2x2 b)
        {
            Matrix2x2 res = new Matrix2x2();
            for (int i = 0; i < 4; i++)
            {
                res.matrix[i / 2, i % 2] = a.matrix[i / 2, i % 2] + b.matrix[i / 2, i % 2];
            }
            return res;
        }
        public static Matrix2x2 operator -(Matrix2x2 a, Matrix2x2 b)
        {
            Matrix2x2 res = new Matrix2x2();
            for (int i = 0; i < 4; i++)
            {
                res.matrix[i / 2, i % 2] = a.matrix[i / 2, i % 2] - b.matrix[i / 2, i % 2];
            }
            return res;
        }
        public static Matrix2x2 operator *(Matrix2x2 a, Matrix2x2 b)
        {
            Matrix2x2 res = new Matrix2x2();
            for (int i = 0; i < 4; i++)
            {
                res.matrix[i / 2, i % 2] = a.matrix[i / 2, 1] * b.matrix[1, i % 2] + a.matrix[i / 2, 0] * b.matrix[0, i % 2];
            }
            return res;
        }
        public static Matrix2x2 operator *(Matrix2x2 a, double b)
        {
            Matrix2x2 res = new Matrix2x2();
            for (int i = 0; i < 4; i++)
                res.matrix[i / 2, i % 2] = a.matrix[i / 2, i % 2] * b;
            return res;
        }
        public static Matrix2x2 operator *(double b, Matrix2x2 a)
        {
            Matrix2x2 res = new Matrix2x2();
            for (int i = 0; i < 4; i++)
                res.matrix[i / 2, i % 2] = a.matrix[i / 2, i % 2] * b;
            return res;
        }
        public static Matrix2x2 operator /(Matrix2x2 a, double b)
        {
            Matrix2x2 res = new Matrix2x2();
            for (int i = 0; i < 4; i++)
                res.matrix[i / 2, i % 2] = a.matrix[i / 2, i % 2] / b;
            return res;
        }
        public static bool operator ==(Matrix2x2 b,Matrix2x2 a)
        {
            for (int i =0; i < 4; i++)
            {
                if (a.matrix[i / 2, i % 2] != b.matrix[i / 2, i % 2])
                    return false;
            }
            return true;
        }
        public static bool operator !=(Matrix2x2 a, Matrix2x2 b)
        {
            for (int i = 0; i < 4; i++)
            {
                if (a.matrix[i / 2, i % 2] != b.matrix[i / 2, i % 2])
                    return true;
            }
            return false;
        }
        public double det()
        {
            return (matrix[0, 0] * matrix[1, 1] - matrix[0, 1] * matrix[1, 0]);
        }
        public void transpon()
        {
            double swapper = matrix[0, 1];
            matrix[0, 1] = matrix[1, 0];
            matrix[1, 0] = swapper;
        }
        public Matrix2x2 InverseMatrix()
        {
            double d = det();
            if (d == 0)
            {
                throw new Exception("InverseMatrixDoesNotExist");
            }
            Matrix2x2 MAD = new Matrix2x2(matrix[1, 1], -matrix[1, 0], -matrix[0, 1], matrix[0, 0]);
            MAD.transpon();
            return (MAD * (1 / d));
        }
    }
}