﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW11
{
    /**

* @author Timur Abdullin

* 11-912

* Task 11

*/
    class HT11
    {
        static int Fact(int l)
        {
            int x = 1;
            for (int i = 1; i < l; i++)
            {
                x *= i;
            }
            return x;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Write n");
            double n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Write x");
            double x = Convert.ToInt32(Console.ReadLine());
            double P = 1;
            for (int i = 1; i < n + 1; i++)
                P *= (Math.Pow(x, i) + Fact(i));
            Console.Write(P);
            Console.ReadKey();
        }
    }
}
