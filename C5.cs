﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW22
{
    class CT22
    {
        static bool IsMatrixThreeDiag(int[,] arr,int m, int n)
        {
            if (m != n)
                return false;
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    //на трех диагоналях могут быть нули, главное чтобы вне диагоналей были только нули
                    if (j >= i - 1 && j <= i + 1)
                    {
                        if (arr[i, j] == 0)
                            return false;
                    }
                    else
                        if (arr[i, j] != 0)
                        return false;                    
                }
            }
            return true;
        }
        static void Main(string[] args)
        {
            int m = int.Parse(Console.ReadLine());
            int n = int.Parse(Console.ReadLine());
            int[,] arr = new int[m, n];
            for (int i = 0; i < m; i++)
            {
                string[] s = Console.ReadLine().Split(' ');
                for (int j = 0; j < n; j++)
                {
                    arr[i, j] = int.Parse(s[j]);
                }
            }
            bool answer = IsMatrixThreeDiag(arr, m, n);
            if (answer)
                Console.WriteLine("Yes");
            else
                Console.WriteLine("No");
            Console.ReadKey();
        }
    }
}
