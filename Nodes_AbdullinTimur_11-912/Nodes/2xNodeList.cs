﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nodes
{
    class DoubleNodeList<T> : ICustomCollection<T> where T : IComparable<T>
    {
        DoubleNode<T> Head;
        public DoubleNodeList (T[] nums)
        {
            int k = nums.Length - 1;
            DoubleNode<T> node = new DoubleNode<T>(nums[k], null,null);
            for (int i = k - 1; i >= 0; i--)
            {
                node = new DoubleNode<T>(nums[i], node, null);
            }
            Head = node;
            node = node.NextNode;
            node.PrevNode = Head;
            while(node.NextNode != null)
            {
                node.NextNode.PrevNode = node;
                node = node.NextNode;
            }
        }
        public void Clear()
        {
            Head = null;
        }
        public int Size()
        {
            if (IsEmpty())
                return 0;
            DoubleNode<T> node = Head;
            int size = 0;
            while (node != null)
            {
                node = node.NextNode;
                size++;
            }
            return size;
        }
        public bool IsEmpty()
        {
            return (Head == null);
        }
        public bool Contains(T hi)
        {
            DoubleNode<T> node = Head;
            while (node != null)
            {
                if (node.Data.CompareTo(hi) == 0)
                    return true;
                node = node.NextNode;
            }
            return false;
        }
        /// <summary>
        /// Нахождение индекса по значению элемента
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int IndexOf(T data)
        {
            int k = 0;
            DoubleNode<T> node = Head;
            while (!node.Data.Equals(data) && node.NextNode != null)
            {
                node = node.NextNode;
                k++;
            }
            return k;
        }
        public void Add(T data)
        {
            DoubleNode<T> node = Head;
            while (node.NextNode != null)
                node = node.NextNode;
            DoubleNode<T> newNode = new DoubleNode<T>(data,null, node);
            node.NextNode = newNode;
        }
        public void AddRange(T[] data)
        {
            DoubleNode<T> node = Head;
            while (node.NextNode != null)
                node = node.NextNode;
            for (int i = 0; i < data.Length; i++)
            {
                node.NextNode = new DoubleNode<T>(data[i],null,node);
                node = node.NextNode;
            }
        }
        public void Remove(T data)
        {
            if (Head.Data.Equals(data))
            {
                Head = Head.NextNode;
                return;
            }
            DoubleNode<T> node = Head;
            while (!node.NextNode.Data.Equals(data))
            {
                if (node.NextNode == null)
                {
                    return;
                }
                node = node.NextNode;
            }
            if (node.NextNode.NextNode != null)
            {
                node.NextNode = node.NextNode.NextNode;
                node.NextNode.PrevNode = node;
            }
            else
                node.NextNode = null;

        }
        public void RemoveAll(T data)
        {
            while (Head.Data.Equals(data))
                Head = Head.NextNode;
            DoubleNode<T> h = Head;
            bool isEnteredSecondCycle;
            while (h.NextNode != null)
            {
                isEnteredSecondCycle = false;
                while (h.NextNode != null && h.NextNode.Data.Equals(data))
                {
                    isEnteredSecondCycle = true;
                    if (h.NextNode.NextNode != null)
                        h.NextNode = h.NextNode.NextNode;
                    else
                        h.NextNode = null;
                }
                if (isEnteredSecondCycle)
                    h.NextNode.PrevNode = h;
                h = h.NextNode;
            }
        }
        public void RemoveAt(int Index)
        {
            if (Index > Size())
                return;
            if (Index == 0)
            {
                Head = Head.NextNode;
                Head.PrevNode = null;
                return;
            }
            DoubleNode<T> node = Head;
            for (int i = 0; i < Index-1; i++)
                node = node.NextNode;
            node.NextNode = node.NextNode.NextNode;
            node.NextNode.PrevNode = node;
        }
        public void Insert(int Index, T item)
        {
            if (Index > Size() - 1)
                return;
            if (Index < 0)
                throw new Exception("???");
            DoubleNode<T> node = Head;
            for (int i = 0; i < Index-1; i++)
                node = node.NextNode;
            node.NextNode = new DoubleNode<T>(item, node.NextNode, node);
        }
        public void Reverse()
        {
            DoubleNode<T> node = Head;
            while (node.NextNode != null)
                node = node.NextNode;
            node.NextNode = Head;
            Head.PrevNode = node;
            DoubleNode<T> Temp;
            while (node != Head)
            {
                Temp = node.PrevNode;
                node.PrevNode = node.NextNode;
                node.NextNode = Temp;
                node = node.NextNode;
            }
            Head = Head.PrevNode;
            node.PrevNode = node.NextNode;
            node.NextNode = null;
            Head.PrevNode = null;
        }
        public void WriteNode()
        {
            DoubleNode<T> node = Head;
            while (node != null)
            {
                Console.Write(node.Data + " ");
                node = node.NextNode;
            }
            Console.WriteLine();
        }
    }
}
