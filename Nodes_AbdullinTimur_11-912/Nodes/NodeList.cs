﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nodes
{
    class NodeList<T> : ICustomCollection<T> where T : IComparable<T>
    {
        Node<T> Head;
        public NodeList (T[] nums)
        {
            int k = nums.Length - 1;
            Node<T> node = new Node<T>(nums[k]);
            for (int i = k - 1; i >= 0; i--)
            {
                node = new Node<T>(nums[i], node);
            }
            Head = node;
        }
        public void Clear()
        {
            Head = null;
        }
        public int Size()
        {
            if (IsEmpty())
                return 0;
            Node<T> node = Head;
            int size = 0;
            while (node != null)
            {
                node = node.NextNode;
                size++;
            }
            return size;
        }
        public bool IsEmpty()
        {
            return (Head == null);
        }
        public bool Contains(T hi)
        {
            Node<T> node = Head;
            while (node != null)
            {
                if (node.Data.CompareTo(hi) == 0)
                    return true;
                node = node.NextNode;
            }
            return false;
        }
        public void Add(T data)
        {
            Node<T> node = Head;
            while (node.NextNode != null)
                node = node.NextNode;
            Node<T> newNode =new Node<T>(data);
            node.NextNode = newNode;
        }
        public void AddRange(T[] data)
        {
            Node<T> node = Head;
            while (node.NextNode != null)
                node = node.NextNode;
            for(int i =0; i < data.Length; i++)
            {
                node.NextNode = new Node<T>(data[i]);
                node = node.NextNode;
            }
        }
        public void Remove(T data)
        {
            if (Head.Data.Equals(data))
            {
                Head = Head.NextNode;
                return;
            }
            Node<T> node = Head;
            while (!node.NextNode.Data.Equals(data))
            {
                if (node.NextNode == null)
                    return;
                node = node.NextNode;
            }
            node.NextNode = node.NextNode.NextNode;
        }
        public void RemoveAll(T data)
        {
            while (Head.Data.Equals(data))
                Head = Head.NextNode;
            Node<T> h = Head;
            while (h.NextNode != null)
            {
                while (h.NextNode != null && h.NextNode.Data.Equals(data))
                {
                    if (h.NextNode.NextNode != null)
                        h.NextNode = h.NextNode.NextNode;
                    else
                        h.NextNode = null;
                }
                h = h.NextNode;
            }
        }
        public void RemoveAt(int Index)
        {
            if (Index > Size())
                return;
            if (Index == 0)
            {
                Head = Head.NextNode;
                return;
            }
            Node<T> node = Head;
            for (int i = 0; i < Index-1; i++)
                node = node.NextNode;
            node.NextNode = node.NextNode.NextNode;
        }
        public void Reverse()
        {
            Node<T> node = Head;
            Node<T> next = null;
            Node<T> prev = null;
            while (node != null)
            {
                next = node.NextNode;
                node.NextNode = prev;
                prev = node;
                node = next;
            }
            Head = prev;
        }
        public void Insert(int Index, T item)
        {
            if (Index > Size() - 1)
                return;
            if (Index < 0)
                throw new Exception("???");
            Node < T > node = Head;
            for (int i = 0; i < Index-1; i++)
                node = node.NextNode;
            node.NextNode = new Node<T>(item, node.NextNode);
        }
        public int IndexOf(T data)
        {
            int k = 0;
            Node<T> node = Head;
            while(!node.Data.Equals(data) && node.NextNode != null)
            {
                node = node.NextNode;
                k++;
            }
            return k;
        }
        public void WriteNode()
        {
            Node<T> node = Head;
            while (node != null)
            {
                Console.Write(node.Data + " ");
                node = node.NextNode;
            }
            Console.WriteLine();
        }
    }
}
