﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nodes
{
    class Node<T>
    {
            public T Data { get; set; }
            public Node<T> NextNode { get; set; }
            public Node(T data)
            {
                Data = data;
                NextNode = null;
            }
            public Node(T data, Node<T> nextNode)
            {
                Data = data;
                NextNode = nextNode;
            }
    }
}
