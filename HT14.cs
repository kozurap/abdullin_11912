﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW14
{
    class HT14
    {
        static void Main(string[] args)
        {
            double x = Convert.ToDouble(Console.ReadLine());
            double eps = 0.000000001;
            int i = 2;
            double f = x;
            double y1;
            double y2 = x;
            do
            {
                y1 = y2;
                f *= ((x * x)*(2*(i-1)-1) / (2 * i -1)); //зачем это (2*(i-1)-1) в числителе - лишнее
                if (i % 2 == 0)
                    y2 = y1 - f;
                else y2 = y1 + f;
                i++;
            }
            while (Math.Abs(y2 - y1) > eps);
            Console.WriteLine(y2);
            Console.WriteLine(Math.Atan(x));
            Console.ReadKey();
        }
    }
}
