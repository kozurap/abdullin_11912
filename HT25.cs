﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW25
{

    /**

    * @author Abdullin Timur

    * 11-912

    * Task 25

*/
    class HT25
    {
        static void Main(string[] args)
        {
            int m = int.Parse(Console.ReadLine());
            int n = int.Parse(Console.ReadLine());
            int[,] arr = new int[m, n];
            if (m != n)
            {
                Console.WriteLine("Матрица не квадратная");
                Console.ReadKey();
                Environment.Exit(0);
            }
            int[] prom = new int[m - 1];
            int[] answ = new int[m - 1];
            if (m == 1)
            {
                Console.Write("Нет паралельных диагоналей");
                Console.ReadKey();
                Environment.Exit(0);
            }

            for (int i = 0; i < m; i++)
            {
                string[] s = Console.ReadLine().Split(' ');
                for (int j = 0; j < n; j++)
                {
                    arr[i, j] = int.Parse(s[j]);
                }
            }
            int max = arr[0, 1];
            for (int i = 1; i < m; i++)
            {
                int a = 0;
                int sum = 0;
                for (int j = i; j < m; j++)
                {
                    prom[a] = arr[j,a];
                    sum += arr[j, a];
                    if (sum >= max)
                    {
                        for (int g = 0; g < m-1; g++)
                            answ[g] = prom[g];
                        max = sum;
                    }
                    a++;
                }
            }
            for (int i = 1; i < m; i++)
            {
                int a = 0;
                int sum = 0;
                for (int j = i; j < m; j++)
                {
                    prom[a] = arr[a, j];
                    sum += arr[a,j];
                    if (sum >= max)
                    {
                        max = sum;
                        for (int g = 0; g <= m-1; g++)
                            answ[g] = prom[g];
                    }
                    a++;
                }
            }
            for (int i = 0; i < m-1; i++)
                Console.Write(answ[i] + " ");
            Console.WriteLine();
            Console.WriteLine(max);
            Console.ReadKey();
        }
    }
}
