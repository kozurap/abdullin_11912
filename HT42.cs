﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW42
{
    /* @author Abdullin Timur
11-912
Task 42*/
    class HT42
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            int length = str.Length;
            char[] res = new char[length + length / 4];
            int status = 1;
            bool SecondEnterance = false;
            int j = 0;
            int save = 1 ;
            for (int i =0; i < length; i++)
            {
                switch (status)
                {
                    case 1:
                        if (str[i] == 't')
                        {
                            status = 2;
                        }
                        else
                        {
                            res[i+j] = str[i];
                        }
                        break;
                    case 2:
                        if (str[i] == 'r')
                            status = 3;
                        else
                        {
                            res[i+j] = str[i];
                            res[i - 1+j] = 't';
                            status = 1;
                        }
                        break;
                    case 3:
                        if (str[i] == 'u')
                            status = 4;
                        else
                        {
                            res[i+j] = str[i];
                            res[i - 1+j] = 'r';
                            res[i - 2+j] = 't';
                            status = 1;
                        }
                        break;
                    case 4:
                        if (str[i] == 'e')
                            if (SecondEnterance == true)
                            {
                                status = 5;
                                save = i;
                            }
                            else
                            {
                                SecondEnterance = true;
                                res[i + j] = str[i];
                                res[i - 1 + j] = 'u';
                                res[i - 2 + j] = 'r';
                                res[i - 3 + j] = 't';
                                status = 1;
                            }
                        else
                        {
                            res[i] = str[i];
                            res[i - 1 + j] = 'u';
                            res[i - 2 + j] = 'r';
                            res[i - 3 + j] = 't';
                            status = 1;
                        }
                        break;
                    case 5:
                        res[i +j] = 'e';
                        res[i - 1 + j] = 's';
                        res[i - 2 + j] = 'l';
                        res[i - 3 + j] = 'a';
                        res[i - 4 + j] = 'f';
                        SecondEnterance = false;
                        if (str[i] != 't')
                        {
                            res[i + 1 + j] = str[i];
                            status = 1;
                        }
                        else
                            status = 2;
                        j++;
                        break;
                };
            }
            if (status == 5)
            {
                res[save + 1 + j] = 'e';
                res[save + j] = 's';
                res[save - 1 + j] = 'l';
                res[save - 2 + j] = 'a';
                res[save - 3 + j] = 'f';
            }
            for (int i = 0; i < res.Length; i++)
                Console.Write(res[i]);
            Console.ReadKey();
        }
    }
}
