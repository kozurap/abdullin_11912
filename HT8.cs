﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW8
{
    /**

* @author Timur Abdullin

* 11-912

* Task 08

*/
    class HT8
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());

            for (int y =2*n; y > -1; y--)
            {
                for (int x = 0; x<2*n+1; x++)
                {
                    //почему Math.Pow(x - n, 2), а не (x-n)*(x-n)? 
                    if (Math.Pow(x - n, 2) + Math.Pow(y - n, 2)  <= Math.Pow(n, 2))
                        Console.Write('0');
                    else Console.Write('*');
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
