﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW39
{
    class Program
    {
        static int LekGrafCount(string str)
        {
            int sum = 0;
            for (int i = 0; i < str.Length; i++)
                sum += Convert.ToInt32(str[i]);
            return sum;
        }
        static void QuickSort(int start, int last, string[] arr)
        {
            int i = start;
            int[] arrlek = new int[arr.Length];
            for (int g =0;g<arr.Length;g++)
                arrlek[g] = LekGrafCount(arr[g]);
            int j = last;
            int OpEl = (arrlek[(last + start) / 2]); /* Медиана */
            int temp;
            string tempstr; /* Переменые для замены */
            do
            {
                while (arrlek[i] < OpEl)
                    i++;
                while (arrlek[j] > OpEl)
                    j--;
                if (i <= j)
                {
                    temp =(arrlek[i]);
                    tempstr = arr[i];
                    (arrlek[i]) = (arrlek[j]);
                    arr[i] = arr[j];
                    (arrlek[j]) = temp;
                    arr[j] = tempstr;
                    i++;
                    j--;
                }
            } while (i < j);
            if (j > start)
                QuickSort(start, j, arr);
            if (i < last)
                QuickSort(i, last, arr);
        }
        static void Main(string[] args)
        {
            string[] arr =(Console.ReadLine().Split(';'));
            QuickSort(0, arr.Length-1, arr);
            for (int i = 0; i < arr.Length; i++)
                Console.Write(arr[i] + ";");
            Console.ReadKey();
        }
    }
}
