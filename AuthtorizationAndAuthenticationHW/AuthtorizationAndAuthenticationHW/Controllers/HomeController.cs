﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AuthtorizationAndAuthenticationHW.Models;
using Microsoft.AspNetCore.Authorization;

namespace AuthtorizationAndAuthenticationHW.Controllers
{
    public class HomeController : Controller
    {
            [Authorize]
            public IActionResult Index() 
            { 
                return Content(User.Identity.Name);
            }
    }
}