﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW41
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[26];
            string str = Console.ReadLine();
            int length = str.Length;
            for (int i =0; i < length; i++)
            {
                char ch = char.ToLower(str[i]);
                if(ch!=' ')
                    arr[(Convert.ToInt32(ch)) - 97]++;
            }
            for (int i =0; i < 26; i++)
            {
                if (arr[i] != 0) /* Я не знаю, надо ли не встреченные буквы выводить, если надо, то без этой строчки просто */
                    Console.WriteLine("Count of " + (char)(i + 97) + " = " + arr[i]);
            }
            Console.ReadKey();
        }
    }
}
