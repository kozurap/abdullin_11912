﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW20
{
    /**

* @author Timur Adbullin

* 11-912

* Task 20

*/
    class Program
    {
        static void Main(string[] args)
        {
            /* IEnumerable - это интерфейс имееющий один единственный метод, 
            который позволяет перебирать элементы коллекции в цикле foreach
            Другими словами, здесь создается перичислитель для целочисленной коллекции. 
            
            /** int k = 0;
             int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
             IEnumerable < Int32 > unique = arr.Distinct();
             Console.WriteLine("Все уникальные элементы массива");
             foreach(int i in unique)
             {
                 k++;
                 Console.Write(i + " ");
             }
             Console.WriteLine();
             if (arr.Length == k)
                 Console.WriteLine("Массив уникальный");
             else Console.WriteLine("Массив не уникальный");
             Console.ReadKey(); */ // Оно неэффективное, но веселое, и еще все уникальные эл-ты выводит
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            
            Array.Sort(arr);
            for (int i = 1; i < arr.Length; i++)
                if(arr[i-1] == arr[i])
                {
                    Console.WriteLine("Массив не уникальный");
                    Environment.Exit(0);
                }
            Console.WriteLine("Массив уникальный");
            Console.ReadKey();
        }
    }
}
