﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P7
{
    class P5
    {
        static void Main(string[] args)
        {
            int k = int.Parse(Console.ReadLine());
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            int[] temp = new int[k];
            int c = 0;
            for (int j = (arr.Length - k); j < arr.Length; j++)
            {
                temp[c] = arr[j];
                c++;
            }
            for (int i = arr.Length - 1 - k; i >= 0; i--)
            {
                arr[i + k] = arr[i];
            }
            for(int i = 0; i < k; i++)
            {
                arr[i] = temp[i];
            }
            for(int i =0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + " ");
            }
            Console.ReadKey();
        }
    }
}
