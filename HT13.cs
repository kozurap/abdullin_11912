﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW13
{
    class HT13
    {
        static void Main(string[] args)
        {
            string ncatch = Console.ReadLine();
            int n = 0;
            while (ncatch.Length != 6)
            {
                Console.WriteLine("Неверно введен номер билета");
                ncatch = Console.ReadLine();
            }
            try
            {
                n = Convert.ToInt32(ncatch);
            }
            catch (FormatException)
            {
                Console.WriteLine("Ошибка.Вы ввели не число");
                Console.ReadKey();
                Environment.Exit(0);
            }
            int s1 = 0;
            int s2 = 0;
            for (int i = 0; i < 3; i++)
            {
                s2 += n % 10;
                    n = n / 10;
            }
            for (int i = 0; i < 3; i++)
            {
                s1 += n % 10;
                n = n / 10;
            }
            if (s1 == s2)
                Console.WriteLine("Билет счастливый");
            else Console.WriteLine("Билет не счастивый");
            Console.ReadKey();
        
        }
    }
}
