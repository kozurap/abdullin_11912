﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW31
{
    /*
    @author Abdullin Timur
    11-912
    Task 
    */
    public class IsEveryElementInMatrixDividedBy3
    {
        public bool Check (int[,] arr,int lines, int columns)
        {
            int[] facecheck = new int[columns];
            bool WhoKnows = false;
            for (int i =0; i<lines; i++)
            {
                for (int j = 0; j < columns; j++)
                    facecheck[j] = arr[i, j];
                WhoKnows = AreAllElementsOfLineDividedBy3(facecheck);
                if (WhoKnows == true)
                    break;
            }
            return WhoKnows;
        }
        private static bool AreAllElementsOfLineDividedBy3(int[] arr)
        {
            bool WhouldYouKindlyBeDividedByThree = true;
            for (int i = 0; i < arr.Length; i++)
                if (arr[i] % 3 != 0)
                {
                    WhouldYouKindlyBeDividedByThree = false;
                    break;
                }
            return WhouldYouKindlyBeDividedByThree;
        }
    }
    class HT31
    {
        static bool AreAllElementsOfLineDividedBy3 (int[] arr)
        {
            bool WhouldYouKindlyBeDividedByThree = true;
            for (int i = 0; i<arr.Length;i++)
                if (arr[i]%3 != 0)
                {
                    WhouldYouKindlyBeDividedByThree = false;
                    break;
                }
            return WhouldYouKindlyBeDividedByThree;
        }
        static void Main(string[] args)
        {
            int m = int.Parse(Console.ReadLine());
            int n = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            bool answ = true;
            IsEveryElementInMatrixDividedBy3 check = new IsEveryElementInMatrixDividedBy3();
            int[,,] arr = new int[m, n, b];
            for (int x = 0; x < m; x++)
                for (int y = 0; y < b; y++)
                    for (int z = 0; z < n; z++)

                        arr[x, y, z] = int.Parse(Console.ReadLine());
            for(int x = 0; x < m; x++)
            {
                int[,] checker = new int[b,n];
                for (int z=0; z < n; z++)
                {
                    for (int y = 0; y < b; y++)
                        checker[z,y] = arr[x, y, z];
                }
                answ = check.Check(checker, n,b);
                if (answ == false)
                {
                    Console.WriteLine("Нет");
                    Console.ReadKey();
                    return ;
                }
            }
            for (int y =0; y < b; y++)
            {
                int[,] checker = new int[n, m];
                for (int z =0; z < n; z++)
                {
                    for (int x = 0; x < m; x++)
                        checker[z, x] = arr[x, y, z];
                }
                answ = check.Check(checker, n,m);
                if (answ == false)
                {
                    Console.WriteLine("Нет");
                    Console.ReadKey();
                    return;
                }
            }
            for (int z =0; z < n; z++)
            {
                int[,] checker = new int[b, m];
                for (int y =0; y < b; y++)
                {
                    for (int x = 0; x < m; x++)
                        checker[y, x] = arr[x, y, z];
                }
                answ = check.Check(checker, b, m);
                if (answ == false)
                {
                    Console.WriteLine("Нет");
                    Console.ReadKey();
                    return;
                }
            }
            Console.WriteLine("Да");
            Console.ReadKey();
        }
    }
}
