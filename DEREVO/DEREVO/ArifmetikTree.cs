﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEREVO
{
    public class ATree
    {
        public double Eval(ATreeNode root)
        {
            double s=0;
            ATreeNode r = root;
                if (r.Left != null && r.Right != null )
                    switch (r.Key)
                    {
                        case "*":
                        s += Eval(r.Left) * Eval(r.Right); ;
                            break;
                        case "/":
                        s += Eval(r.Left) / Eval(r.Right); ;
                            break;
                        case "-":
                        s += Eval(r.Left) - Eval(r.Right); ;
                            break;
                        case "+":
                        s += Eval(r.Left) + Eval(r.Right); ;
                            break;
                    }
            if (!IsOper(r))
                s += Convert.ToDouble(r.Key);
            return s;
        }
        /// <summary>
        /// Вывод в ширину
        /// </summary>
        public void PrintDepth(ATreeNode root)
        {
            PrintLevel(new List<ATreeNode>() { root });
        }

        /// <summary>
        /// Вывод одного уровня дерева
        /// </summary>
        /// <param name="list"></param>
        private void PrintLevel(List<ATreeNode> list)
        {
            if (list == null || list.Count == 0)
                return;
            var nextLevel = new List<ATreeNode>();
            foreach (var node in list)
            {
                if (node != null)
                {
                    Console.Write(node.Key + "  ");
                    if (node.Left != null)
                        nextLevel.Add(node.Left);
                    if (node.Right != null)
                        nextLevel.Add(node.Right);
                }
            }
            Console.WriteLine();
            PrintLevel(nextLevel);
        }
        private bool IsOper(ATreeNode a)
        {
            return (a.Key == "-" || a.Key == "+" || a.Key == "*" || a.Key == "/");
        }
    }

    /// <summary>
    /// Узел дерева выражений
    /// </summary>
    
}
