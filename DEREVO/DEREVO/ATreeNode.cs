﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEREVO
{
    public class ATreeNode
    {
        public ATreeNode(string key, string left = null, string right = null)
        {
            Key = key;
            if (left != null)
                Left = new ATreeNode(left) { Parent = this };
            if (right != null)
                Right = new ATreeNode(right) { Parent = this };
        }

        /// <summary>
        /// Данные узла
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// Левый потомок
        /// </summary>
        public ATreeNode Left { get; set; }
        /// <summary>
        /// Правый потомок
        /// </summary>
        public ATreeNode Right { get; set; }
        /// <summary>
        /// Родитель
        /// </summary>
        public ATreeNode Parent { get; set; }

        //private List<string> operations =
        //    new List<string> { "+", "-", "/", "*" };
        ///// <summary>
        ///// Является ли ключ узла операцией
        ///// </summary>
        ///// <returns></returns>
        //public bool IsOper() 
        //{
        //    return operations.Contains(Key);
        //}
    }
}
