﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEREVO
{
    class CommonTreeNode<T>
    {
        public int Key { get; set; }
        public T Data { get; set; }
        public List<CommonTreeNode<T>> Children { get; set; }
        public CommonTreeNode<T> Parent { get; set; }
    }
}
