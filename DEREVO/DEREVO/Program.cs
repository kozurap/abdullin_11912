﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEREVO
{
    class Program
    {
        public class ATreeRunner
        {
            public void Run()
            {
                var root = new ATreeNode("-", "+", "/");
                root.Left.Left = new ATreeNode("*", "2", "4");
                root.Left.Left.Parent = root.Left;
                root.Left.Right =
                    new ATreeNode("/", "9", "3") { Parent = root.Left };
                root.Right.Left =
                        new ATreeNode("*", "4", "5") { Parent = root.Right };
                root.Right.Right = new ATreeNode("2") { Parent = root.Right };

                var aTree = new ATree();
                aTree.PrintDepth(root);
                Console.WriteLine(aTree.Eval(root));
            }
        }
        static void Main(string[] args)
        {
            var runner = new ATreeRunner();
            runner.Run();
            Console.ReadKey();
        }
    }
}
