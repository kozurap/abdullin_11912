﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEREVO
{
    public class BTreeNode<T>
    {
        public int Key { get; set; }
        public T Data { get; set; }
        public BTreeNode<T> LeftChild { get; set; }
        public BTreeNode<T> RightChild { get; set; }
        public BTreeNode<T> Parent { get; set; }
    }
}
