﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW7
{
    /**

* @author Timur Abdullin

* 11-912

* Task 07

*/
    class HT7
    {
        static void Main(string[] args)
        {
            for (int i = 6; i < 33; i += 3)
            {
                Console.Write(i);
                Console.Write(" ");
            }
            Console.ReadKey();
        }
    }
}
