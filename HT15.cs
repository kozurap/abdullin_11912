﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cosx_in_eps
{
    class HT15
    {
        static void Main(string[] args)
        {
            double x = Convert.ToDouble(Console.ReadLine());
            double eps = 0.000000001;
            int i = 0;
            double f = 1;
            double y1;
            double y2 = 1;
            do
            {
                y1 = y2;
                i++;
                f *= ((x*x)/ (2*i*(2*i-1)));
                if (i % 2 == 0)
                    y2 = y1 + f;
                else y2 = y1 - f;
            }
            while (Math.Abs(y2 - y1) > eps);
            Console.WriteLine(y2);
            Console.WriteLine(Math.Cos(x));
            Console.ReadKey();
        }
    }
}
