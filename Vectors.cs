﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW50
{
    class Vectors
    {
        (double, double) A, B;
        private (double, double,double,double) Get
        {
            get
            {
                return (A.Item1,A.Item2,B.Item1,B.Item2);
            }
            set
            {
                A.Item1 = value.Item1;
                A.Item2 = value.Item2;
                B.Item1 = value.Item3;
                B.Item2 = value.Item4;
            }
        }
        public Vectors()
        {
            A = (0, 0);
            B = (0, 0);
        }
        public Vectors((double, double) a, (double, double) b)
        {
            A = a;
            B = b;
        }
        public static (double, double) operator +(Vectors a1, Vectors a2)
        {
            (double, double) res;
            (double, double) Vec1 = a1.GetCoords();
            (double, double) Vec2 = a2.GetCoords();
            res.Item1 = Vec1.Item1 + Vec2.Item1;
            res.Item2 = Vec1.Item2 + Vec2.Item2;
            return res;
        }
        public (double, double) GetCoords()
        {
            (double, double) res;
            res.Item1 = B.Item1 - A.Item1;
            res.Item2 = B.Item2 - A.Item2;
            return res;
        }
        public static (double, double) operator -(Vectors a1, Vectors a2)
        {
            (double, double) res;
            (double, double) Vec1 = a1.GetCoords();
            (double, double) Vec2 = a2.GetCoords();
            res.Item1 = Vec1.Item1 + Vec2.Item1;
            res.Item2 = Vec1.Item2 + Vec2.Item2;
            return res;
        }
        public static (double, double) operator *(Vectors a, double b)
        {
            (double, double) res = a.GetCoords();
            res.Item1 *= b;
            res.Item2 *= b;
            return res;
        }
        public static (double, double) operator *(double b, Vectors a)
        {
            (double, double) res = a.GetCoords();
            res.Item1 *= b;
            res.Item2 *= b;
            return res;
        }
        public static (double,double) operator /(Vectors a, double b)
        {
            (double, double) res = a.GetCoords();
            res.Item1 /= b;
            res.Item2 /= b;
            return res;
        }
        public static double operator *(Vectors a, Vectors b)
        {
            double cos = a.GetCos(b);
            return (a.length() * b.length()) * cos;
        }
        public double GetCos(Vectors a)
        {
            (double, double) Vec1 = a.GetCoords();
            Vectors b = new Vectors(A, B);
            (double, double) Vec2 = b.GetCoords();
            return (Vec1.Item1 * Vec2.Item1 + Vec1.Item2 * Vec2.Item2) /
                (a.length() * b.length());
        }
        public override string ToString()
        {
            (double, double) res = GetCoords();
            return ("(" + res.Item1 + ";" + res.Item2 + ")");
        }
        public double length()
        {
            (double, double) Vec1 = GetCoords();
            return (Math.Sqrt(Vec1.Item1 * Vec1.Item1 + Vec1.Item2 * Vec1.Item2));
        }
        public static bool operator ==(Vectors a, Vectors b)
        {
            (double, double) Vec1 = a.GetCoords();
            (double, double) Vec2 = b.GetCoords();
            if (Vec1 == Vec2)
                return true;
            else return false;
        }
        public static bool operator !=(Vectors a, Vectors b)
        {
            (double, double) Vec1 = a.GetCoords();
            (double, double) Vec2 = b.GetCoords();
            if (Vec1 == Vec2)
                return false;
            else return true;
        }
    }
}
