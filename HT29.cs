﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW29
{
    /* 
    @author Abdullin Timur
    11-912
    Task  29 
    */
    class HT29
    {
        //матрица в метод передается по ссылке. Исходная матрица arr и так будет изменена. Метод может быть void.
        static int[,] ZeroTrianglesInMatrix(int[,] arr, int n)
        {
            int k = 0;
            for (int i = 0; i < n; i++)
            {
                k++;
                for (int j = k; j <= 2 * n - k; j++)
                    arr[i, j] = 0;
            }
            k = n;
            for (int i = n + 1; i < 2 * n + 1; i++)
            {
                for (int j = k; j <= 2 * n - k; j++)
                    arr[i, j] = 0;
                k--;
            }
            return arr;
        }
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[,] arr = new int[2 * n + 1, 2 * n + 1];
            for (int i = 0; i < 2 * n + 1; i++)
            {
                string[] s = Console.ReadLine().Split(' ');
                for (int j = 0; j < 2 * n + 1; j++)
                {
                    arr[i, j] = int.Parse(s[j]);
                }
            }
            arr = ZeroTrianglesInMatrix(arr, n);
            for (int i =0; i< 2 * n + 1; i++)
            {
                for (int j = 0; j < 2 * n + 1; j++)
                    Console.Write(arr[i, j] + " ");
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
