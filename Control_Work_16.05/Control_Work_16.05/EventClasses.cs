﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_Work_16._05
{
    public class BankEventArgs : EventArgs
    {
        public string Zastroy { get; }
        public string Pokupatel { get; }
        public DateTime today;
        public BankEventArgs(string message1, string message2)
        {
            Zastroy = message1;
            Pokupatel = message2;
            today = DateTime.Today;
        }
    }
    class Bank
    {
        public event EventHandler<BankEventArgs> Action;
        public string name;
        public Bank(string n)
        {
            name = n;
        }
        public void StepaOformlyaetIpoteku()
        {
            Action?.Invoke(this, new BankEventArgs(("Ипотека оформлена, вам перечислены деньги"), ("Ваш запрос на ипотеку одобрен: ")));
        }
    }
    class Zastroy
    {
        private void Mark(object w, BankEventArgs e)
        {
            Console.WriteLine("Наименования банка " + ((Bank)w).name);
            Console.WriteLine(e.Zastroy);
        }
        public void Register(Bank w)
        {
            w.Action += Mark;
        }
    }
    class Pokupatel
    {
        private void IpotekaDana(object w, BankEventArgs e)
        {
            Console.WriteLine("Отправитель - " + ((Bank)w).name + ". " + e.Pokupatel + e.today);
        }
        public void Register(Bank w)
        {
            w.Action += IpotekaDana;
        }
    }

}
