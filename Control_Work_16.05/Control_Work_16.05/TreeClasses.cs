﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_Work_16._05
{
    class BTreeNode<T>
    {
        public int Key { get; set; }
        public T Data { get; set; }
        public BTreeNode<T> LeftChild { get; set; }
        public BTreeNode<T> RightChild { get; set; }
        public BTreeNode<T> Parent { get; set; }
        public BTreeNode(int key, T data, BTreeNode<T> l, BTreeNode<T> r, BTreeNode<T> p)
        {
            Key = key;
            Data = data;
            LeftChild = l;
            RightChild = r;
            Parent = p;
        }
        public BTreeNode()
        {

        }
    }
    class BinarySearchTree<T>
    {
        //!!!! Сигнатура методов может быть изменена, обсудим
        public BTreeNode<T> root;
        /// <summary>
        /// возвращает значение корня дерева
        /// изначально надо было вернуть позицию
        /// </summary>
        public T Root()
        {
            return root.Data;
        }
        public BTreeNode<T> FindElement(int p, BTreeNode<T> r)
        {
            if (p < r.Key)
                if (r.LeftChild != null)
                    return (FindElement(p, r.LeftChild));
                else return null;
            else if (p > r.Key)
                if (r.RightChild != null)
                    return FindElement(p, r.RightChild);
                else return null;
            else return r;
        }

        /// <summary>
        /// возвращает значение «родителя» для вершины в позиции p
        /// изначально надо было вернуть позицию
        /// </summary>
        public BTreeNode<T> FindElementByPos(int p)
        {
            List<int> way = new List<int>();
            while (p > 1)
            {
                way.Insert(0, p);
                p = p / 2;
            }
            if (p != 1)
                throw new ArgumentNullException();
            var r = root;
            for (int i = 0; i < way.Count; i++)
            {
                if (way[i] % 2 == 0)
                    r = r.LeftChild;
                else r = r.RightChild;
            }
            return r;
        }
        public T Parent(int p)
        {
            if (p < 2)
                throw new ArgumentException();
            return (FindElementByPos(p).Parent.Data);

        }

        /// <summary>
        /// возвращает значение «самого левого сына» для вершины в позиции p.
        /// изначально надо было вернуть позицию
        /// </summary>
        public T LeftMostChild(int p)
        {
            BTreeNode<T> b = FindElementByPos(p);
            while (b.LeftChild != null)
                b = b.LeftChild;
            return b.Data;
        }

        /// <summary>
        /// возвращает значение «правого брата» для вершины в позиции p.
        /// изначально надо было вернуть позицию
        /// </summary>
        public T RightSibling(int p)
        {
            if (p < 2)
                throw new ArgumentException();
            int way = 0;
            while (p > 1)
            {
                way++;
                p = p / 2;
            }
            if (p != 1)
                throw new ArgumentNullException();
            var r = root;
            for (int i = 0; i < way; i++)
            {
                if (r.RightChild != null)
                    r = r.RightChild;
                else if (r.LeftChild != null)
                    r = r.LeftChild;
                else throw new Exception("Нету брата правого");//Ну там зарофлить можно и все равно найти, даже в имбалансном челе
            }
            return r.Data;
        }

        /// <summary>
        /// возвращает элемент дерева (хранимую информацию) для вершины в позиции p.
        /// </summary>
        public T Element(int p)
        {
            return FindElementByPos(p).Data;
        }

        /// <summary>
        /// проверяет, является ли p позицией внутренней вершины (не листа)
        /// </summary>
        public bool IsInternal(int p)
        {
            return (FindElementByPos(p).LeftChild != null || FindElementByPos(p).RightChild != null);
        }

        /// <summary>
        /// проверяет, является ли p позицией листа дерева.
        /// </summary>
        public bool IsExternal(int p)
        {
            return (FindElementByPos(p).LeftChild == null && FindElementByPos(p).RightChild == null);
        }

        /// <summary>
        /// проверяет, является ли p позицией корня.
        /// </summary>
        public bool IsRoot(int key)
        {
            if (root == null)
                throw new ArgumentNullException();
            return (root.Key == key);
        }

        /// <summary>
        /// Поиск элемента в дерве
        /// </summary>
        public bool Find(int key)
        {
            if (FindElement(key, root) == null)
                return false;
            return true;
        }

        /// <summary>
        /// добавление в дерево значения 
        /// </summary>
        public void Insert(T data, int key, BTreeNode<T> r)
        {
            if (root == null)
            {
                root = new BTreeNode<T>
                {
                    Data = data,
                    Key = key,
                    Parent = null,
                    LeftChild = null,
                    RightChild = null
                };
                return;
            }

            if (r.Key > key)
                if (r.LeftChild == null)
                    r.LeftChild = new BTreeNode<T>
                    {
                        Data = data,
                        Key = key,
                        Parent = r,
                        LeftChild = null,
                        RightChild = null
                    };
                else
                    Insert(data, key, r.LeftChild);
            else if (r.Key < key)
                if (r.RightChild == null)
                    r.RightChild = new BTreeNode<T>
                    {
                        Data = data,
                        Key = key,
                        Parent = r,
                        LeftChild = null,
                        RightChild = null
                    };
                else
                    Insert(data, key, r.RightChild);
            else if (r.Key == key)
                r.Data = data;
        }

        /// <summary>
        /// удаление узла, в котором хранится значение
        /// </summary>
        public void Remove(int key)
        {
            if (root == null)
                throw new ArgumentNullException();
            var r = root;
            bool search = true;
            while (search)
            {
                if (r.Key > key)
                    if (r.LeftChild == null)
                        throw new ArgumentNullException();
                    else r = r.LeftChild;
                else if (r.Key < key)
                    if (r.RightChild == null)
                        throw new ArgumentNullException();
                    else r = r.RightChild;
                else if (r.Key == key)
                    search = false;
            }
        }

        //https://learnc.info/adt/binary_tree_traversal.html вывод деревьев

        /// <summary>
        /// Вывод в глубину прямой
        /// Прямой (pre-order)        
        /// Посетить корень    
        /// Обойти левое поддерево    
        /// Обойти правое поддерево
        /// </summary>
        public void PreOrderPrint(Program.Del del)
        {
            PreOrderPrintOneStep(root,del);
        }

        private void PreOrderPrintOneStep(BTreeNode<T> root,Program.Del del)
        {
            if (root == null) return;
            del(root.Data.ToString());
            PreOrderPrintOneStep(root.LeftChild,del);
            PreOrderPrintOneStep(root.RightChild,del);
        }
    }
}
