﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Control_Work_16._05
{
    class Program
    {
        public void RunLinq()
        {
            var products = new List<Product>
            {
                new Product { Id = 1, Name = "Аквариум 10 литров" },
                new Product { Id = 2, Name = "Аквариум 20 литров" },
                new Product { Id = 3, Name = "Аквариум 50 литров" },
                new Product { Id = 4, Name = "Аквариум 100 литров" },
                new Product { Id = 5, Name = "Аквариум 200 литров" },
                new Product { Id = 6, Name = "Фильтр" },
                new Product { Id = 7, Name = "Термометр" }
            };

            var prices = new List<Price>
            {
                new Price { Id = 1, ProductId = 1, Sum = 100, IsActual = false },
                new Price { Id = 2, ProductId = 1, Sum = 123, IsActual = true },
                new Price { Id = 3, ProductId = 2, Sum = 234, IsActual = true },
                new Price { Id = 4, ProductId = 3, Sum = 532, IsActual = true },
                new Price { Id = 5, ProductId = 4, Sum = 234, IsActual = true },
                new Price { Id = 6, ProductId = 5, Sum = 534, IsActual = true },
                new Price { Id = 7, ProductId = 5, Sum = 124, IsActual = false },
                new Price { Id = 8, ProductId = 6, Sum = 153, IsActual = true },
                new Price { Id = 9, ProductId = 7, Sum = 157, IsActual = true }
            };
            //1ое задание
            var payment = new List<Payment>
            {
                new Payment("Аквариум 200 литров", 3),
                new Payment("Термометр",1),
                new Payment("Фильтр",4),
                new Payment("Аквариум 50 литров",2),
                new Payment("Аквариум 10 литров",8)//Тут я мог бы рандомом выбирать цифру, брать цифру как id в product, и вытаскивать строку, но я не думаю что это так нужно
            };
            //2ое задание
            var sch = from x in prices
                      from y in products
                      from z in payment
                      where x.IsActual && x.ProductId == y.Id && y.Name == z.Name
                      select new { z.Name,y.Id, z.Count, Price = x.Sum * z.Count };
            sch.OrderBy(x => x.Name);
            decimal itogo = 0;
            foreach (var e in sch)
            {
                Console.WriteLine(e.Name+ " x " + e.Count + " : " + e.Price);
                itogo += e.Price;
            }
            Console.WriteLine("Итого: " + itogo);
            //3е задание
            var sums = prices.Select(x => (x.ProductId, x.Sum)).GroupBy(x => x.Item1);
            foreach (var e in sums)
            {
                Console.WriteLine(e.Average(x => x.Sum));
            }
            //4ое задание
            var sales = new List<Sale>
            {
                new Sale {ProductId = 5,Product2Name = "Фильтр",countOf2=2, percent = 0.15M},
                new Sale {ProductId = 4,Product2Name = "Фильтр",countOf2=2, percent = 0.1M},
                new Sale {ProductId = 3,Product2Name = "Фильтр",countOf2=1, percent = 0.05M},
                new Sale {ProductId = 2,Product2Name = "Фильтр",countOf2=1, percent = 0.05M},
                new Sale {ProductId = 1,Product2Name = "Фильтр",countOf2=1, percent = 0.05M}
            };
            //5ое задание
            var pricepolicy = from x in prices
                              from y in products
                              from z in sales
                              where x.IsActual && x.ProductId == y.Id && y.Id == z.ProductId
                              select new { y.Name, z.countOf2,x.Sum, PriceBefore = (x.Sum + (153 * z.countOf2)), PriceAfter = (x.Sum + (153 * z.countOf2)) * (1 - z.percent), y.Id, z.Product2Name };
            foreach (var e in pricepolicy)
            {
                Console.WriteLine(e.Name + " + "+e.countOf2 +" "+ e.Product2Name + " " + e.PriceBefore + " " + e.PriceAfter);
            }
            //6ое задание, которое я отказываюсь делать другим способом, потому что если делать с помощью тех методов, что вы дали спустя неделю после кр, кучу коллекций надо переделывать, чтобы они были одного вида.
            List<string> checkNames = new List<string>();
            foreach(var e in payment)
            {
                for (int i =0; i< e.Count; i++)
                {
                    checkNames.Add(e.Name);
                }
            }
            decimal totalwithsale = 0;
            decimal totalwithoutsale = 0;
            int k =0;
            int j = 0;
            foreach (var e in pricepolicy.Reverse())
            {
                while (checkNames.Contains(e.Name))
                {
                    checkNames.Remove(e.Name);
                    bool flag = false;
                        for (k = 0; k < e.countOf2; k++)
                        {
                            if(checkNames.Contains(e.Product2Name))
                              checkNames.Remove(e.Product2Name);
                            else
                            {
                                flag = true;
                                break;
                            }
                            if(flag)
                                for (j = 0; j < k; j++)
                                {
                                    checkNames.Add(e.Product2Name);
                                }
                        }
                    if (!flag)
                        totalwithsale += e.PriceAfter;
                    else
                        totalwithsale += e.Sum;
                    if (flag)
                        totalwithoutsale += e.Sum;
                    else
                        totalwithoutsale += e.PriceBefore;
                }
            }
            var pr = from x in products
                     from y in prices
                     from z in checkNames
                     where z == x.Name && y.IsActual && x.Id == y.ProductId
                     select (y.Sum); 
            foreach(var e in pr)
            {
                totalwithoutsale += e;  
                totalwithsale += e;
            }
            Console.WriteLine("Total with sale " + totalwithsale + ". Total without sale " + totalwithoutsale);
        }
        public void RunEvent()
        {
            Bank b = new Bank("Sberbank");
            Zastroy z = new Zastroy();
            Pokupatel p = new Pokupatel();
            z.Register(b);
            p.Register(b);
            b.StepaOformlyaetIpoteku();
        }
        public void RunReflection()
        {
            var assembly = Assembly.LoadFrom(@"C:\Users\Тимур\source\repos\ClassForReflection\ClassForReflection\bin\Debug\ClassForReflection.dll");
            Type type = assembly.GetType("ClassForReflection.Employer");
            object Hi = System.Activator.CreateInstance(type);
            var flag = BindingFlags.Static | BindingFlags.Public;
            MethodInfo method = type.GetMethod("HeyGuys", flag);
            method.Invoke(Hi, null);
            var flag2 = BindingFlags.NonPublic | BindingFlags.Instance;
            FieldInfo field = type.GetField("gender", flag2);
            field.SetValue(Hi, "Male");
            Console.WriteLine(field.GetValue(Hi));
            var flag3 = BindingFlags.Public | BindingFlags.Instance;
            PropertyInfo property = type.GetProperty("Age", flag3);
            property.SetValue(Hi, 33);
            Console.WriteLine(property.GetValue(Hi));
            Type type2 = assembly.GetType("ClassForReflection.Manager");
            object Hi2 = System.Activator.CreateInstance(type2);
            MethodInfo methodReg = type2.GetMethod("Register");
            MethodInfo methodFind = type2.GetMethod("FindReplacement");
            EventInfo ev = type.GetEvent("Action");
            methodReg.Invoke(Hi2, new object[] { Hi });
            MethodInfo eventActivator = type.GetMethod("VacationTime");
            eventActivator.Invoke(Hi, null);
        }
        public delegate void Del(string s);
        private static void MessageColor(string s)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(s);
        }
        public void RunTrees()
        {
            Del d = Program.MessageColor;
            BinarySearchTree<int> bTree = new BinarySearchTree<int>();
            BTreeNode<int> root = new BTreeNode<int>(8, 1, null, null, null);
            BTreeNode<int> l = new BTreeNode<int>();
            BTreeNode<int> r = new BTreeNode<int>();
            l = new BTreeNode<int>(4, 2, new BTreeNode<int>(2, 3, null, null, l), new BTreeNode<int>(9, 4, null, null, l), root);
            r = new BTreeNode<int>(17, 5, new BTreeNode<int>(7, 6, null, null, r), new BTreeNode<int>(35, 7, null, null, r), root);
            root = new BTreeNode<int>(8, 1, l, r, null);
            bTree.root = root;
            bTree.PreOrderPrint(d);
            Console.ResetColor();
        }
            static void Main(string[] args)
        {
            Program p = new Program();
            p.RunLinq();
            Console.WriteLine();
            p.RunEvent();
            Console.WriteLine();
            p.RunReflection();
            Console.WriteLine();
            p.RunTrees();
            Console.ReadKey();
        }
    }
}
