﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Control_Work_16._05
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Price
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public decimal Sum { get; set; }
        public bool IsActual { get; set; }
    }
    public class Payment
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public Payment(string n, int c)
        {
            Name = n;
            Count = c;
        }
    }
    public class Sale
    {
        public int ProductId { get; set; }
        public string Product2Name { get; set; }
        public int countOf2;
        public decimal percent { get; set; }
    }

}
