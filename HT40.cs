﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW40
{
    /* @author Abdullin Timur
11-912
Task 40
*/
    class Program
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            int length = str.Length;
            int count = 0;
            for (int i =0; i< length; i++)
            {
                char ch = str[i];
                if (ch!=' ')
                if (ch == char.ToUpper(ch))
                    count++;
            }
            Console.WriteLine(count);
            Console.ReadKey();
        }
    }
}
