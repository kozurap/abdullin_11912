﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW55_HW56
{
    class Program
    {
        static void Main(string[] args)
        {
            Players p1 = new Players("George", 18, 10);
            Players p2 = new Players("John", 19, -1);
            Players p3 = new Players("Fedor", 20, 42);
            Players p4 = new Players("Gerasim", 17, 11);
            Players[] arr = { p1, p2, p3, p4 };
            Array.Sort(arr, new PlayerComp("Name"));
            for(int i =0; i<4; i++)
            {
                Console.WriteLine(arr[i]);
            }
            Array.Sort(arr, new PlayerComp("Age"));
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine(arr[i]);
            }
            Array.Sort(arr, new PlayerComp("Number"));
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine(arr[i]);
            }
            Console.ReadKey();
        }
    }
}
