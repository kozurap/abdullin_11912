﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW55_HW56
{
    public class Team
    {
        static List<Players> P;
        public void Add(Players player)
        {
            P.Add(player);
        }
        public Players this[int uniquenum]
        {
            get
            {
                foreach (Players e in P)
                {
                    if (e.PNum == uniquenum)
                        return e;
                }
                return null;
            }
        }
        public Players this[string name]
        {
            get
            {
                foreach (Players e in P)
                {
                    if (e.PName == name)
                        return e;
                }
                return null;
            }
        }
        public void DeletePlayer (Players p)
        {
            if (P.Contains(p))
            {
                P.Remove(p);
            }
            else
                throw new Exception("Player isn't in a Team");
        }
    }
}
