﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW49
{
    class ComplexNumbers
    {
        private double a, b;
        public ComplexNumbers()
        {
            a = 0;
            b = 0;
        }
        public ComplexNumbers(double re, double im)
        {
            a = re;
            b = im;
        }
        private (double, double) GetnSet
        {
            get
            {
                return (a, b);
            }
            set
            {
                a = value.Item1;
                b = value.Item2;
            }
        }
        public ComplexNumbers add(ComplexNumbers z)
        {
            ComplexNumbers res = new ComplexNumbers();
            res.a = a + z.a;
            res.b = b + z.b;
            return res;
        }
        public void addInSelf(ComplexNumbers z)
        {
            a += z.a;
            b += z.b;
        }
        public ComplexNumbers sub(ComplexNumbers z)
        {
            ComplexNumbers res = new ComplexNumbers();
            res.a = a - z.a;
            res.b = b - z.b;
            return res;
        }
        public void subInSelf(ComplexNumbers z)
        {
            a -= z.a;
            b -= z.b;
        }
        public ComplexNumbers multNum(double x)
        {
            ComplexNumbers res = new ComplexNumbers();
            res.a = a * x;
            res.b = b * x;
            return res;
        }
        public void multNumInSelf(double x)
        {
            a *= x;
            b *= x;
        }
        public ComplexNumbers mult(ComplexNumbers z)
        {
            ComplexNumbers res = new ComplexNumbers();
            res.a = a * z.a - b * z.b;
            res.b = a * z.b + b * z.a;
            return res;
        }
        public void multInSelf ( ComplexNumbers z)
        {
            double x = a * z.a - b * z.b;
            b = a * z.b + b * z.a;
            a = x;
        }
        public ComplexNumbers div (ComplexNumbers z)
        {
            ComplexNumbers res = new ComplexNumbers();
            res.a = (a * z.a + b * z.b) / (z.a * z.a + z.b * z.b);
            res.b = (a * z.a - b * z.b) / (z.a * z.a + z.b * z.b);
            return res;
        }
        public void divInSelf(ComplexNumbers z)
        {
            double x = (a * z.a + b * z.b) / (z.a * z.a + z.b * z.b);
            b = (a * z.a - b * z.b) / (z.a * z.a + z.b * z.b);
            a = x;
        }
        public double length()
        {
            return Math.Sqrt(a * a + b * b);
        }
        public override string ToString()
        {
            if (b > 0 && a != 0)
                return (a + "+" + b + "i");
            else if (b == 0 && a != 0)
                return (Convert.ToString(a));
            else if (a == 0 && b != 0)
                return (Convert.ToString(b + "i"));
            else if (a == 0 && b == 0)
                return (Convert.ToString(0));
            else return (a + "" + b + "i");
        }
        public double arg()
        {
            return Math.Acos(a / Math.Sqrt(a * a + b * b));
        }
        public ComplexNumbers pow(double power)
        {
            ComplexNumbers res = new ComplexNumbers();
            res.a = Math.Pow(Math.Sqrt(a * a + b * b), power)* Math.Cos(Math.Acos(a/ Math.Sqrt(a * a + b * b))*power);
            res.b = Math.Pow(Math.Sqrt(a * a + b * b), power) * Math.Sin(Math.Asin(b / Math.Sqrt(a * a + b * b)) * power);
            return res;
        }
        public bool Equals(ComplexNumbers z)
        {
            if (a == z.a && b == z.b)
                return true;
            else return false;
        }
        public static ComplexNumbers operator +(ComplexNumbers z1, ComplexNumbers z2)
        {
            ComplexNumbers res = new ComplexNumbers();
            res.a = z1.a + z2.a;
            res.b = z1.b + z2.b;
            return res;
        }
        public static ComplexNumbers operator -(ComplexNumbers z1, ComplexNumbers z2)
        {
            ComplexNumbers res = new ComplexNumbers();
            res.a = z1.a - z2.a;
            res.b = z1.b - z2.b;
            return res;
        }
        public static ComplexNumbers operator +(ComplexNumbers z1, double d)
        {
            return new ComplexNumbers(z1.a + d, z1.b);
        }
        public static ComplexNumbers operator +(double d,ComplexNumbers z1)
        {
            return new ComplexNumbers(z1.a + d, z1.b);
        }
        public static ComplexNumbers operator -(ComplexNumbers z1, double d)
        {
            return new ComplexNumbers(z1.a - d, z1.b);
        }
        public static ComplexNumbers operator -(double d, ComplexNumbers z1)
        {
            return new ComplexNumbers(d-z1.a, z1.b);
        }
        public static ComplexNumbers operator *(ComplexNumbers z1, ComplexNumbers z2)
        {
            return new ComplexNumbers(z1.a * z2.a - z1.b * z2.b, z1.a * z2.b + z1.b * z2.a);
        }
        public static ComplexNumbers operator *(ComplexNumbers z1, double d)
        {
            return new ComplexNumbers(z1.a * d, z1.b * d);
        }
        public static ComplexNumbers operator *(double d, ComplexNumbers z1)
        {
            return new ComplexNumbers(z1.a * d, z1.b * d);
        }
        public static ComplexNumbers operator /(ComplexNumbers z1, ComplexNumbers z)
        {
            ComplexNumbers res = new ComplexNumbers();
            res.a = (z1.a * z.a + z1.b * z.b) / (z.a * z.a + z.b * z.b);
            res.b = (z1.a * z.a - z1.b * z.b) / (z.a * z.a + z.b * z.b);
            return res;
        }
        public static ComplexNumbers operator /(ComplexNumbers z1, double d)
        {
            return new ComplexNumbers(z1.a / d, z1.b / d);
        }
        public static ComplexNumbers operator /(double d, ComplexNumbers z1)
        {
            ComplexNumbers z2 = new ComplexNumbers(d, 0);
            return z2 / z1;
        }
        public static bool operator ==(ComplexNumbers z1, ComplexNumbers z2)
        {
            if (z1.a == z2.a && z1.b == z2.b)
                return true;
            else return false;
        }
        public static bool operator !=(ComplexNumbers z1, ComplexNumbers z2)
        {
            if (z1.a == z2.a && z1.b == z2.b)
                return false;
            else return true;
        }
    }
}