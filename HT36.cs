﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW36
{
    class HT36
    {
        static void QuickSort (int start, int last, int[] arr)
        {
            int i = start;
            int j = last;
            int OpEl = arr[(last + start) / 2]; /* Медиана */
            int temp; /* Переменая для замены */
            do
            {
                while (arr[i] < OpEl)
                    i++;
                while (arr[j] > OpEl)
                    j--;
                if (i <= j)
                {
                    temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                    i++;
                    j--;
                }
            } while (i < j);  
                if (j > start)
                    QuickSort(start, j, arr);
                if (i < last)
                    QuickSort(i, last, arr);
        }
        static void Main(string[] args)
        {
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            QuickSort(0, arr.Length - 1, arr);
            for (int i = 0; i < arr.Length; i++)
                Console.Write(arr[i] + " ");
            Console.ReadKey();
        }
    }
}
