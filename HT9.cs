﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW9
{
    class Program
    {
        static void Main(string[] args)
        {
            double n = Convert.ToInt32(Console.ReadLine());
            double P = 1;
            for (int i =2; i < n+1; i++)
            {
                P *= (1 - (1.0 / i));
            }
            Console.WriteLine(P);
            Console.ReadKey();
        }
    }
}
