﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace matrix1
{
    class Program
    {
        static int[] FuncW(int x, int m, int n, int[,] arr,int[] a)
        {
            int y = 0;
            int b = 0;
            for (int i = x; i <= n; i++)
            {
                if (a[0] != m || a[1]!=i)
                {
                    Console.Write(arr[m, i] + " ");
                    y++;
                }
                b++;
            }
            if (y == 0)
            {
                Console.ReadKey();
                Environment.Exit(0);
            }
            int[] ar = { m, x+b-1 };
            return ar;
        }
        static int[] FuncB(int x, int m, int n, int[,] arr,int[] a)
        {
            int y = 0;
            int b = 0;
            for (int i = n; i >=x; i--)
            {
                if (a[0] != m || a[1]!=i)
                {
                    Console.Write(arr[m, i] + " ");
                    y++;
                }
                b++;
            }
            if (y == 0)
            {
                Console.ReadKey();
                Environment.Exit(0);
            }
            int[] ar = { m, b };
            return ar;
        }
        static int[] FuncD(int x, int m, int n, int[,] arr, int[] a)
        {
            int y = 0;
            int b = 0;
            for (int i = x; i <=m; i++)
            {
                if (a[0] != i || a[1] != n)
                {
                    Console.Write(arr[i, n] + " ");
                    y++;
                }
                b++;
            }
            if (y == 0)
            {
                Console.ReadKey();
                Environment.Exit(0);
            }
            int[] ar = { b, n };
            return ar;
        }
        static int[] FuncU(int x, int m, int n, int[,] arr, int[] a)
        {
            int y = 0;
            int b = 0;
            for (int i = m - 1; i > x; i--)
            {
                if (a[0] != i || a[1] !=n)
                {
                    Console.Write(arr[i, n] + " ");
                    y++;
                }
                b++;
            }
            if (y == 0)
            {
                Console.ReadKey();
                Environment.Exit(0);
            }
            int[] ar = { b, n };
            return ar;
        }
        static void Main(string[] args)
        {
            int m = int.Parse(Console.ReadLine());
            int n = int.Parse(Console.ReadLine());
            int[,] arr = new int[m, n];
            for (int i = 0; i < m; i++)
            {
                string[] s = Console.ReadLine().Split(' ');
                for (int j = 0; j < n; j++)
                {
                    arr[i, j] = int.Parse(s[j]);
                }
            }
            int[] a = { -1, -1};
            int s1 = -1;
            int s2 = m;
            int c1 = -1;
            int c2 = n;
            for (int k =0; k<m*n;k++) 
            {
                if (s2 < s1 || c1 >  c2) 
                    break;
                a=FuncW(k, s1+1, c2-1, arr, a);
                s1++;
                if (c1 > c2 || s1 > s2) 
                    break;
                a=FuncD(k, s2-1, c2-1, arr,a);
                c2--;
                if (s1 > s2 || c1> c2)
                    break;
                a=FuncB(k, s2-1, c2-1, arr,a);
                s2--;
                if (c2 < c1 || s1 > s2)
                    break;
                a=FuncU(k, s2, c1+1, arr,a);
                c1++;
            }
            Console.ReadKey(); 
        }
    }
}
