﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using stud;

namespace Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            var atom = new PeacefullAtom();
            var mchs = new MCHS();
            var firestation = new FireStation();
            mchs.Register(atom);
            firestation.Register(atom);
            atom.MakeSomeElectricity();
            Console.ReadKey();
        }
    }
}
