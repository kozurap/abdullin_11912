﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using stud;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Student student1 = new Student("Filadelfia", "George", DateTime.Now, 5);
            Student student2 = new Student("New York", "John", DateTime.UtcNow, 2);
            int a =student1.CompareTo(student2);
            Assert.AreEqual(1, a);
        }
    }
}
