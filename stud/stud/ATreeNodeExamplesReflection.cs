﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace stud
{
    public class ATreeNodeExamplesReflection
    {
        public void Run()
        {
            /* var root = new ATreeNode("-", "+", "/");
            root.Left.Left = new ATreeNode("*", "2", "4");
            root.Left.Left.Parent = root.Left;
            root.Left.Right =
                new ATreeNode("/", "9", "3") { Parent = root.Left };
            root.Right.Left =
                    new ATreeNode("*", "4", "5") { Parent = root.Right };
            root.Right.Right = new ATreeNode("2") { Parent = root.Right };

            var aTree = new ATree();
            aTree.PrintDepth(root);
            Console.WriteLine(aTree.Eval(root));*/
            var a = Assembly.LoadFrom(@"C:\Users\Тимур\source\repos\DEREVO\DEREVO\bin\Debug\DEREVO.exe");
            Type atreeNode = a.GetType("DEREVO.ATreeNode");
            object root = Activator.CreateInstance(atreeNode, new object[] { "-", "+", "/" });
            PropertyInfo Left = atreeNode.GetProperty("Left");
            PropertyInfo Right = atreeNode.GetProperty("Right");
            PropertyInfo Parent = atreeNode.GetProperty("Parent");
            object LeftFromRoot = Left.GetValue(root);
            object LeftLeftFromRoot = Activator.CreateInstance(atreeNode, new object[] { "*", "2", "4" });
            Left.SetValue(LeftFromRoot, LeftLeftFromRoot );
            Parent.SetValue(LeftLeftFromRoot, LeftFromRoot);
            object LeftRightFromRoot = Activator.CreateInstance(atreeNode, new object[] { "/", "9", "3" });
            Right.SetValue(LeftFromRoot, LeftRightFromRoot);
            Parent.SetValue(LeftRightFromRoot, LeftFromRoot);
            object RightFromRoot = Right.GetValue(root);
            object RightLeftFromRoot = Activator.CreateInstance(atreeNode, new object[] { "*", "4", "5" });
            Left.SetValue(RightFromRoot, RightLeftFromRoot);
            Parent.SetValue(RightLeftFromRoot, RightFromRoot);
            object RightRightFormRoot = Activator.CreateInstance(atreeNode, new object[] { "2", null,null });
            Right.SetValue(RightFromRoot, RightRightFormRoot);
            Parent.SetValue(RightRightFormRoot, RightFromRoot);
            Type Atree = a.GetType("DEREVO.ATree");
            object ATree = Activator.CreateInstance(Atree);
            MethodInfo PD = Atree.GetMethod("PrintDepth");
            PD.Invoke(ATree, new object[] { root });
            var Eval = Atree.GetMethod("Eval");
            var d = Eval.Invoke(ATree, new object[] { root });
            Console.WriteLine(d);
            Console.ReadKey();
        }
    }
}
