﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stud
{
    public class FireStation
    {
        public void Fire(object sender, EmpEventArgs e)
        {
            Console.WriteLine(e.mesForFireStation);
            Console.WriteLine(((PeacefullAtom)sender).CurrentTemp);
        }
        public void Register(PeacefullAtom atom)
        {
            atom.Action += Fire;
        }
    }
}
