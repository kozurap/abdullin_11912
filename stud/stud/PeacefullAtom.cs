﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace stud
{
    public class EmpEventArgs : EventArgs
    {
        public string mesForFireStation { get; }
        public string mesForMCHS { get; }
        public EmpEventArgs(string message1,string message2)
        {
            mesForFireStation = message1;
            mesForMCHS = message2;
        }
    }
    public class PeacefullAtom
    {
        public event EventHandler<EmpEventArgs> Action;
        public int BlowTemp;
        public int CurrentTemp;
        public string name;
        public Timer timer = new Timer(1000);
        public PeacefullAtom()
        {
            BlowTemp = 1000;
            CurrentTemp = 0;
            timer.AutoReset = true; ;
        }
        public void MakeSomeElectricity()
        {
            timer.Elapsed += TempRise;
            timer.Start();
            while (true)
            {
                if (CurrentTemp == BlowTemp)
                {
                    RH8F8();
                    return;
                }
            }
        }
        public void TempRise(Object source, ElapsedEventArgs e)
        {
            CurrentTemp += 100;
        }
        public void RH8F8()
        {
            Action?.Invoke(this, new EmpEventArgs("Ну ядерный взрыв мы конечно не потушим, но постараемся", "МЧС едет, но наши хим.костюмы от ядерного бабаха не спасут, удачи нам выжить"));
        }
    }
}
