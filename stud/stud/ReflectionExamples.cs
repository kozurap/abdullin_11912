﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace stud
{
    public class ReflectionExamples
    {
        public void Run()
        {
            var assembly = Assembly.LoadFrom(@"C:\Users\Тимур\source\repos\stud\stud\bin\Debug\stud.dll");
            foreach(Type t in assembly.ExportedTypes)
            {
                Console.WriteLine(t.Name);
            }
            Type[] types = assembly.GetTypes();
            Type type = assembly.GetType("stud.ClassForReflection");
            object classRedlection = System.Activator.CreateInstance(type);
            object classRedlection2 = System.Activator.CreateInstance(type, new object[] { 15 });
            var flags = BindingFlags.NonPublic | BindingFlags.Instance;
            object  classReflectionNonPublicWithParams = System.Activator.CreateInstance(type, flags,
                null, new object[] { "Hi, my name is George, I am from Filadelphia" }, null);
            Type StringBuilderType = (new StringBuilder()).GetType();
            object sb = Activator.CreateInstance(StringBuilderType);
            sb = (StringBuilder)sb;
            StringBuilder sb2 = (StringBuilder)sb;
            MethodInfo[] met = type.GetMethods();
            MethodInfo method = type.GetMethod("DoSmth");
            method.Invoke(classRedlection, null);
            MethodInfo method2 = type.GetMethod("DoSmthWithPrmAndResult");
            method2.Invoke(classRedlection, new object[] { "hi" });
            var flag = BindingFlags.Static | BindingFlags.NonPublic;
            MethodInfo method3 = type.GetMethod("DoStaticMethod", flag);
            method3.Invoke(classRedlection, null);
            FieldInfo[] allPublicFields = type.GetFields();
            FieldInfo publicField = type.GetField("PublicField");
            object res3 = publicField.GetValue(classRedlection);
            publicField.SetValue(classRedlection, "George");
            object res4 = publicField.GetValue(classRedlection);
            Console.WriteLine(res3.ToString() + ";" + res4.ToString());
            EventInfo[] allEvents = type.GetEvents();
            EventInfo ev = type.GetEvent("TestEvent");
            FieldInfo arrayInfo = type.GetField("IntArray");
            object res6 = arrayInfo.GetValue(classRedlection);
            var res7 = Array.CreateInstance(arrayInfo.FieldType, 5);

        }
    }
}
