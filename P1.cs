﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P1
{
    class P1
    {
        static bool IsTriangleForm(int[,] a, int n)
        {
            bool ch = true, ch1 = true;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                //на диагонали могут быть нули
                    if (i == j && a[i, j] == 0)
                        ch = false;
                    else if (i > j && a[i, j] != 0)
                        ch = false;
                }
            }
            if (ch == false)
            {
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        if (i == j && a[i, j] == 0)
                            ch1 = false;
                        else if (i < j && a[i, j] != 0)
                            ch1 = false;
                    }
                }
            }
            if (ch == true || ch1 == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        static void Main(string[] args)
        {
        }
    }
}
