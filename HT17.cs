﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW17
{
    /**

* @author Timur Adbullin

* 11-912

* Task 20

*/
    class HT17
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] arr = new int[n];
            for (int i = 0; i < n; i++)
            {
                if (i % 2 == 1)
                    arr[i] = 2 * i + 1;
                else arr[i] = -(2 * i + 1);
                Console.Write(arr[i]);
                Console.Write(' ');
            }
            Console.ReadKey();
        }
    }
}
