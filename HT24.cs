﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HT24
{
    class HT24
    {
        static void Main(string[] args)
        {
            string bignum = Console.ReadLine();
            string bignum2 = Console.ReadLine();
            int[] bigboy = new int[bignum.Length];
            int[] bigboy2 = new int[bignum2.Length];
            for (int i = 0; i < bignum.Length; i++)
            {
                char nomnom = bignum[i];
                bigboy[i] = int.Parse(Convert.ToString(nomnom));
            }
            for (int i = 0; i < bignum2.Length; i++)
            {
                char nomnom = bignum2[i];
                bigboy2[i] = int.Parse(Convert.ToString(nomnom));
            }
            int[] answ = new int[bigboy.Length + bigboy2.Length-1];
            for (int i = 0; i < bigboy.Length; i++)
                for (int j = 0; j < bigboy2.Length; j++)
                {
                    answ[answ.Length - 1 - j - i] += bigboy[bigboy.Length-1-i] * bigboy2[bigboy2.Length - 1 - j];
                }    
            for (int i = answ.Length-1; i > 0; i--)
            {
                if (answ[i] >= 10)
                {
                    answ[i - 1] += answ[i] / 10;
                    answ[i] = answ[i] % 10;
                }
            }
            for(int i =0; i < answ.Length; i++)
            {
                if (i == 0 && answ[i] == 0)
                    continue;
                Console.Write(answ[i]);
            }
            Console.ReadKey();
        }
    }
}
