﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW18
{
    /**

* @author Timur Adbullin

* 11-912

* Task 18

*/
    class HT18
    {
        static void Main(string[] args)
        {
            long s = 0;
            Console.WriteLine("Прямой порядок или обратный");
            string sup=Console.ReadLine();
            sup.ToLower();
            while  (sup!="прямой"&& sup != "обратный")
            {
                Console.WriteLine("Неверно введен порядок. Введите либо \"прямой\", \"обратный\"");
                sup = Console.ReadLine();
                sup.ToLower();
            }
            if (sup == "прямой")
            {

                for (int i = 0; i < args.Length; i++)
                {
                    try
                    {
                        s = s * 10 + int.Parse(args[i]);
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Ошибка");
                        Environment.Exit(0);
                    }
                }
            }
            else
            {
                for(int i = args.Length - 1; i >= 0; i--)
                {
                    try
                    {
                        s = s * 10 + int.Parse(args[i]);
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Ошибка");
                        Environment.Exit(0);
                    }
                }
            }
            Console.WriteLine(s);
            Console.ReadKey();
        }
    }
}
