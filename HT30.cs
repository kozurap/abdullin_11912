﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW30
{
    /* 
   @author Abdullin Timur
   11-912
   Task 30
   */
    class HT30
    {
        static double[,] LadderMatrix (double[,] arr,int m)
        {
            double[] FReplace = new double[m];
            int CountOfColumn = -1;
            int CountOfLine = 0;
            int CountOfSucces = 0;
            do
            {
                int PromCount = CountOfColumn + 1;
                int IndexOfNotZeroElement = -1;
                for (int g = PromCount; g < m; g++)
                {
                    CountOfColumn = g;
                    for (int j = CountOfLine; j < m; j++) /* Нахождение первого ненулевого элемента в столбце i в матрице*/
                    {
                        if (arr[j, g] != 0)
                        {
                            IndexOfNotZeroElement = j;
                            break;
                        }
                    }
                    if (IndexOfNotZeroElement != -1)
                        break;

                }
                if (IndexOfNotZeroElement != -1)
                {
                    CountOfSucces++;
                    if (IndexOfNotZeroElement != 0) /*Выполняем Fij */
                    {
                        for (int j = 0; j < m; j++)
                            FReplace[j] = arr[IndexOfNotZeroElement, j];
                        for (int j = 0; j < m; j++)
                            arr[IndexOfNotZeroElement, j] = arr[CountOfLine, j];
                        for (int j = 0; j < m; j++)
                            arr[CountOfLine, j] = FReplace[j];
                    }
                    for (int i = CountOfLine + 1; i < m; i++)
                    {
                        double PromICoC = arr[i, CountOfColumn];
                        for (int j = CountOfColumn; j < m; j++)
                        {
                            arr[i, j] += arr[CountOfLine, j] * -PromICoC / arr[CountOfLine, CountOfColumn];
                        }
                    }
                    CountOfLine++;
                }
            }
            while (CountOfColumn < m - 1);
            if (CountOfSucces == 0)
            {
                Console.WriteLine("Невозможно привести к ступенчатому виду");
                Console.ReadKey();
                Environment.Exit(0);
            }
            return arr;
        }
        static void Main(string[] args)
        {
            int m = int.Parse(Console.ReadLine());
            double[,] arr = new double[m,m];
            for (int i = 0; i < m; i++)
            {
                string[] s = Console.ReadLine().Split(' ');
                for (int j = 0; j < m; j++)
                {
                    arr[i, j] = int.Parse(s[j]);
                }
            }
            arr = LadderMatrix(arr,m);
            for (int i =0; i<m; i++)
            {
                for (int j = 0; j < m; j++)
                    Console.Write(arr[i, j] + " ");
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
