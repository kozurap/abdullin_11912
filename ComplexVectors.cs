﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW49
{
    class ComplexVectors
    {
        //а чем такой вектор отличается от комплексного числа? 
        //Каждая координата вектора должна быть комплексным числом.
        ComplexNumbers z;
        public ComplexVectors()
        {
            z.a = 0;
            z.b = 0;
        }
        public ComplexVectors(double x, double y)
        {
            z.a = x;
            z.b = y;
        }
        public static ComplexVectors operator +(ComplexVectors v1, ComplexVectors v2)
        {
            ComplexVectors res = new ComplexVectors(v1.z.a + v2.z.a, v1.z.b + v2.z.b);
            return res;
        }
        public bool equals(ComplexVectors v1, ComplexVectors v2)
        {
            return (v1.z == v2.z);
        }
        public override string ToString()
        {
            return ("(" + z.a + ";" + z.b + ")");
        }
        public double scalarProduct(ComplexVectors v1)
        {
            return (Math.Sqrt(z.a * z.a + z.b * z.b) * Math.Sqrt(v1.z.a * v1.z.a + v1.z.b * v1.z.b) * ((z.a * z.b + v1.z.a * v1.z.b) /
                (Math.Sqrt(z.a * z.a + z.b * z.b) * Math.Sqrt(v1.z.a * v1.z.a + v1.z.b * v1.z.b))));
        }
    }
}
