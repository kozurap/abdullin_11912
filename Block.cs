﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupaHomework
{
    abstract class Block
    {
        bool visability;
        bool IsPiercable;
        public Block(bool vis, bool pierce)
        {
            visability = vis;
            IsPiercable = pierce;
        }
        public Block() : this(true, false)
        {
        }
        public abstract void durabilityDec();
        public void Destroy()
        {
            visability = false;
            IsPiercable = true;
            Console.WriteLine("Block destroyed");
        }
    }
    class Metal : Block
    {
        bool Immortality;
        public override void durabilityDec()
        {
            if (Immortality == true)
                Console.WriteLine("Can't do that, he is IMMORTAL");
            else
                Console.WriteLine("Durability allreade = 0");
        }
        public  void Destroy()
        {
            base.Destroy();
            Immortality = false;
            Console.WriteLine("Metal block destoyed");
            }
        public Metal(bool Immo) : base()
        {
            Immortality = Immo;
        }
        public Metal() : this(true)
        {
        }
        public string Make_a_Sound()
        {
            return ("*Whack of metal*");
        }
    }
    class Wood : Metal
    {
        int durability;
        public Wood(int dur) : base()
        {
            durability = dur;
        }
        public Wood() : this(1)
        {
        }
        public override void durabilityDec()
        {
            durability--;
            if (durability <= 0)
            {
                Destroy();
                PlayDestoyAnimation();
            }
        }
        public void Destroy()
        {
            base.Destroy();
            durability = -1;
            Console.WriteLine("Wooden block destroyed");
        }
        public void PlayDestoyAnimation()
        {
            Console.WriteLine(@"/ , . , \");
        }
    }
}
