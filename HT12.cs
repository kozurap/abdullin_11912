﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW12
{
    class HT12
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int k = n % 10;
            n = n / 10;
            bool ubv = true;
            while (n > 0)
            {
                if (k >= (n % 10))
                {
                    ubv = false;
                    break;
                }
                k = n % 10;
                n = n / 10;
            }
            if (ubv == true)
                Console.WriteLine("yes");
            else Console.WriteLine("no");
            Console.ReadLine();
        }
    }
}
