﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW37
{
    class HT37 /* Я решил зачем выписывать весь массив в обратном порядке если это можно сделать с любой точки массива */
    {
        static void TiKtoTakoiChtobiEtoDelat(int[] arr,int n)
        {
            if (arr.Length - 1 > n)
                TiKtoTakoiChtobiEtoDelat(arr, n + 1);
            Console.Write(arr[n] + " ");
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Введите массив");
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            Console.WriteLine("С какого индекса массив перевернуть (Нумерация начитается с нуля)");
            int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
                Console.Write(arr[i] + " ");
            TiKtoTakoiChtobiEtoDelat(arr, n);
            Console.ReadKey();
        }
    }
}
