﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW21
{
    class C4
    {
        static bool IsEveryThirdElementCanBeDividedByTree(int[] arr)
        {
            int n = arr.Length;
            bool answer = true;
            for (int i = 1; i <= n; i++)
                if (i % 3 == 0)
                    if (arr[i - 1] % 3 != 0)
                    {
                        answer = false;
                        break;
                    }
            return answer;
        }
        static void Main(string[] args)
        {
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            int n = arr.Length;
            bool div3 = IsEveryThirdElementCanBeDividedByTree(arr);
            if (div3)
            {
                int sum = 0;
                for (int i = 0; i < n; i++)
                    if (arr[i] > 0)
                        sum += arr[i];
                Console.WriteLine(sum);
            }
            else
            {
                int mult = 1;
                for (int i = 0; i < n; i++)
                    if (arr[i] > 0)
                        mult *= arr[i];
                Console.WriteLine(mult);
            }
            Console.ReadKey();
        }
    }
}
