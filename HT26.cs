﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW26
{
    /**

* @author Abdullin Timur

* 11-912

* Task 25

*/
    class HT26
    {
        static void Main(string[] args)
        {
            int m = int.Parse(Console.ReadLine());
            int n = int.Parse(Console.ReadLine());
            int[,] arr = new int[m, n];
            if (m != n)
            {
                Console.WriteLine("Ошибка, Матрица не квадратная");
                Console.ReadKey();
                Environment.Exit(0);
            }
            for (int i = 0; i < m; i++)
            {
                string[] s = Console.ReadLine().Split(' ');
                for (int j = 0; j < n; j++)
                {
                    arr[i, j] = int.Parse(s[j]);
                }
            }
            int max = arr[0, 0];
            for (int i = 0; i < m; i++)
            {
                int sum = 0;
                int a = 0;
                int l;
                for (int j = i; j < m+i; j++)
                {
                    l = j;
                    if (l > m - 1)
                    {
                        l = l % m;
                    }
                    sum += arr[a, l];
                    if (max < sum)
                        max = sum;
                    a++;
                }
            }
            Console.WriteLine(max);
            Console.ReadKey();
        }
    }
}
