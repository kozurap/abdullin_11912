﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2
{
    /**

* @author Timur Abdullin

* 11-912

* Task 06

*/
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                for (int j = 1; j < (4 * n + 1); j++)
                {
                    if (j == (2 * n - i))
                    {
                        for (int g = 2 * n - i; g < 2 * n + i + 1; g++)
                            Console.Write("*");
                    }
                    else Console.Write(" ");
                }
                Console.WriteLine();
            }
            for (int i = 0; i < n; i++)
            {
                for (int j = 1; j < (4 * n + 1); j++)
                {
                    if (j == (n - i) || (j == 3 * n - i))
                    {
                        if (j == (n - i))
                        {
                            for (int g = n - i; g < n + i+1; g++)
                                Console.Write("*");
                            j = n + i;
                        }
                        if (j == (3 * n - i))
                        {
                            for (int g = 3 * n - i; g < 3 * n + i + 1; g++)
                            {
                                Console.Write("*");
                               
                            }
                        }
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine();

            }
            Console.ReadKey();
        }
    }
}