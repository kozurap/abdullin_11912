﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassForReflection
{
    public class EmpEventArgs : EventArgs
    {
        public string mesForManager { get; }
        public string mesForDep { get; }
        public DateTime now;
        public EmpEventArgs(string message1, string message2)
        {
            mesForManager = message1;
            mesForDep = message2;
            now = DateTime.Now;
        }
    }
    public class Employer
    {
        public delegate void EmpHandler(Employer emp, EmpEventArgs e);
        public event EmpHandler Action;
        private string gender;
        public int Age {get;set;}
        public void VacationTime()
        {
            Action?.Invoke(this, new EmpEventArgs("Ищу замену", "Опять он в отпуск уходит"));
        }
        public static void HeyGuys()
        {
            Console.WriteLine("Hello");
        }
    }
    public class Manager
    {
        public void FindReplacemet(Employer emp, EmpEventArgs e)
        {
            Console.WriteLine(e.mesForManager);
            Console.WriteLine(e.now);
            Console.WriteLine(emp.Age);
        }
        public void Register(Employer emp)
        {
            emp.Action += FindReplacemet;
        }
    }
}
